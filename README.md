# 1_NSI_21

Dépôt de fichiers de __première NSI__ Lycée M de Flandre à Gondecourt  
C. Ghesquiere : cecile.ghesquiere@ac-lille.fr  
lien vers le réseau du lycée : [ici](https://194.167.100.29:8443/owncloud/index.php)  


---
![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-008.gif) ![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-008.gif)  

__chapitre 22 : KNN__  
* [Cours ](./Chapitres/c22_knn/k_plus_proches_voisins_2.md)  
 
![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-008.gif) ![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-009.gif)  

__chapitre 21 : Traitement de données en tables__  
* [Cours partie 1 : recherche de données](./Chapitres/c21_traitement_donnees_tables/c21_p1_csv.md)  
    * [TP](./Chapitres/c21_traitement_donnees_tables/tp1_tables.md)  

* [Cours partie 2 : tri et fusion de tableaux](./Chapitres/c21_traitement_donnees_tables/C21_2.md)  


![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-008.gif) ![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-010.gif)  

__chapitre 20 : Réseau__  
* [Cours HTTP](./Chapitres/c20_reseau/c20_cours_http.md)  
    * [exercices](./Chapitres/c20_reseau/exercices/c20_exos_client_serveur.md) 
    * [Utilisations de formulaires](./Chapitres/c20_reseau/C20_1_formulaire.md) 
* [cours TCP](./Chapitres/c20_reseau/c20_2_cours_tcp.md)
    * [tp1 Filius : table ARP](./Chapitres/c20_reseau/tp1_filius/tp1_filius.md)
    * [tp2 : TCP](./Chapitres/c20_reseau/tp2_tcp/tp2_tcp.md)

![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-009.gif) ![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-001.gif)  

__chapitre 19 : Algorithme glouton__  
* [Cours](./Chapitres/c19_glouton/cours_algo_glouton.md)  
* [TP](./Chapitres/c19_glouton/TP_haie_fleurie.md)  

![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-009.gif) ![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-002.gif)  

__chapitre 18 : Recherche dichotomique__  
* [Cours](./Chapitres/c18_dichotomie/cours/cours_dichotomie.md)  
* [TP](./Chapitres/c18_dichotomie/tp_dichotomie/tp_dichotomie.md)  

![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-009.gif) ![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-003.gif)  

__chapitre 17 : Codage des caractères__  
* [activité introductive](./Chapitres/c17_codage_caracteres/1_ac_intro/c17_caracteres_ac_intro.md) : et le [corrigé](./Chapitres/c17_codage_caracteres/1_ac_intro/c17_caracteres_ac_intro_correction.md) 
* [cours](./Chapitres/c17_codage_caracteres/c17_caracteres_cours.md) 
* [TP](./Chapitres/c17_codage_caracteres/3_tp_caracteres/c15_tp_caracteres.md) 

![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-009.gif) ![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-004.gif)  

__chapitre 16 : Interaction homme machine__  
* Partie 1  
    * Découverte du Javascript : [cours](./Chapitres/c16_ihm/c16_p1_decouvertejs/c16_decouverte_js.md)  
* Partie 2  
    * Programmation événementielle : [cours](./Chapitres/c16_ihm/c16_p2_evenementiel/c16_evenementiel.md)  
    * TP : [sujet](./Chapitres/c16_ihm/c16_p2_evenementiel/tp_evenements/C16_2_tp_evenements.md)  


![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-009.gif) ![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-005.gif)  

__chapitre 15 : langages HTML CSS__  
    * pour les débutants : [activité](./Chapitres/c15_html/html_css_niveau_debutant/HTML_5_CSS3.pdf)  --> [lien le dossier à compléter pour le projet](./Chapitres/c15_html/html_css_niveau_debutant/projet_3_semaines)
    * pour les plus avancés : [activité](./Chapitres/c15_html/html_css_niveau_avance/C15_html_avance.md)  
    
![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-009.gif) ![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-006.gif)  

__chapitre 14 : les dictionnaires__

![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-009.gif) ![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-007.gif)  

__chapitre 13 : les tris__

* cours Jupyter notebook : [dossier à télécharger](./Chapitres/c13_tris/C13_jupy_ent.zip) : enregistrer ce dossier puis faites glisser les fichiers dans l'application Jupyter de l'ENT.
 * version en ligne [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2FCecGhesq%2F1_nsi_mdf/HEAD?filepath=Chapitres%2FC13_tris%2FC13_tp_tri_insertion.ipynb)
* [TP](/Chapitres/c13_tris/C13_TP_tris.md) : Déroulement des tris 

![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-009.gif) ![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-008.gif)  

__chapitre 12 : les tuples__  

* Cours [en ligne](./Chapitres/c12_tuples/C12_tuples.md) à travailler pour le 2/2 ou  [![](./img/pdf.jpg)](./Chapitres/c12_tuples/C12_tuples.pdf)  
* TP [en ligne](./Chapitres/c12_tuples/c12_tp.md) ou [![](./img/pdf.jpg)](./Chapitres/c12_tuples/C12_TP_tuples.pdf)  


![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-009.gif) ![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-009.gif)  

__chapitre 11 : Système d'exploitation__ 

* [activité introductive](./Chapitres/c11_systeme_exploitation/ac_intro/C11_os_intro.md) ou [pdf](./Chapitres/c11_systeme_exploitation/ac_intro/C11_os_intro.pdf)  
* cours 11_1 : [Systèmes d'exploitation](./Chapitres/c11_systeme_exploitation/cours/C11_1_os.md)  ou  [![](./img/pdf.jpg)](.Chapitres/c11_systeme_exploitation/cours/C11_1_os.pdf)  
* [TP Shell](.Chapitres/c11_systeme_exploitation/tp_shell/tp_shell_weblinux.md) ou   [![](./img/pdf.jpg)](./Chapitres/c11_systeme_exploitation/tp_shell/tp_shell_weblinux.pdf)  
* Cours 11_2 : [droits des utilisateurs](./Chapitres/c11_systeme_exploitation/cours/C11_2_droits_utilisateurs.md) ou [![](./img/pdf.jpg)](./Chapitres/c11_systeme_exploitation/cours/C11_2_droits_utilisateurs.pdf)  
* TP Droits : [sujet en ligne](./Chapitres/c11_systeme_exploitation/tp_droits/C11_2_TP_droits.md) avec le fichier à télécharger [ici](./Chapitres/c11_systeme_exploitation/tp_droits/frozen-bubble-2_2_0.zip)
---
![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-009.gif) ![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-010.gif)    

__Chapitre 10 : Mise au point des programmes__  

* Cours [en ligne](./Chapitres/c10_mise_au_point/C10_mise_au_point.md)  ou [![](./img/pdf.jpg)](./Chapitres/c10_mise_au_point/C10_mise_au_point.pdf) 
* TP [en ligne](./Chapitres/c10_mise_au_point/C10_TP_mise_au_point_programmes.md)  ou [![](./img/pdf.jpg)](./Chapitres/c10_mise_au_point/C10_TP__mise_au_point.pdf) 

---
![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-001.gif)  

__Chapitre 9 : les tableaux : type list en PYTHON__  
    * Le type list : cours 1 [en ligne](./Chapitres/c9_listes/C9 cours1/cours/C9_1_liste_tab.md)  
    * TP_1 : [en ligne](./Chapitres/c9_listes/C9 cours1/TP/C9_1_TP_liste.md)  
    * Listes en compréhension ; listes 2 D [en ligne](./Chapitres/c9_listes/C9 cours2 listes en Python/cours/C9_2_listes_en_comprehension.md)  ou [pdf](./Chapitres/c9_listes/C9 cours2 listes en Python/cours/C9_2_listes_en_comprehension.pdf)  
    * TP_2 : [en ligne](./Chapitres/c9_listes/C9 cours2 listes en Python/TP/C9_2_TP_liste_en_comp.md)  ou [pdf](./Chapitres/c9_listes/C9 cours2 listes en Python/TP/C9_2_TP_liste_en_comp.pdf)

![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-002.gif)  

__Chapitre 8 : Parcours séquentiel dans une chaîne__  
    * Architecture d'un ordinateur : cours [en ligne](./Chapitres/c8_architecture_ordi/C8_1/C8_1.md) ou [![](./img/pdf.jpg)](./Chapitres/c8_architecture_ordi/C8_1/C8_1.pdf)  
    * Algèbre de Boole et circuits combinatoires: cours [en ligne](./Chapitres/c8_architecture_ordi/C8_2/C8_2.md) ou [![](./img/pdf.jpg)](./Chapitres/c8_architecture_ordi/C8_2/C8_2.pdf)   
    * TP1 logique [en ligne](./Chapitres/c8_architecture_ordi/C8_2/TP/TP1_CV.md) ou [![](./img/pdf.jpg)](./Chapitres/c8_architecture_ordi/C8_2/TP/TP1_CV.pdf)  
    * TP2 circuits combinatoires [en ligne](./Chapitres/c8_architecture_ordi/C8_2/TP/TP2_circuits_combinatoires_eleves.md) ou [![](./img/pdf.jpg)](./Chapitres/c8_architecture_ordi/C8_2/TP/TP2_circuits_comb.pdf)  ; voir corrigé additionneur [ici](https://framagit.org/CecGhesq/1_nsi_21/-/blob/main/Chapitres/c8_architecture_ordi/C8_2/TP/add_correction.md)   
    * Exercices sur les parties 1 et 2 du chapitre : [en ligne](https://framagit.org/CecGhesq/1_nsi_21/-/blob/main/Chapitres/c8_architecture_ordi/C8_2/exercices/Exercices_C8_1_2.md)  
    * Le langage machine : cours [en ligne](./Chapitres/c8_architecture_ordi/C8_3/8_3_cours.md) ou [![](./img/pdf.jpg)](./Chapitres/c8_architecture_ordi/C8_3/C8_3_langage_ùachine.pdf)    
    * TP3 utilisation de la M999: télécharger le [dossier zippé](./Chapitres/c8_architecture_ordi/C8_3/M999.zip) de la machine puis dézipper-le. Sujet d'activité [en ligne](./Chapitres/c8_architecture_ordi/C8_3/C8_3_TP_langage_assembleur.md) ou [![](./img/pdf.jpg)](./Chapitres/c8_architecture_ordi/C8_3/C8_3/C8_3_TP_langage_machine.pdf)  
    * correction [multiplication](./Chapitres/c8_architecture_ordi/C8_3/tant_que_correction.pdf)  

![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-003.gif)  

__Chapitre 7 : Parcours séquentiel dans une chaîne__  
    * cours : cours [en ligne](./Chapitres/c7_parcours_sequentiel/c7_for_string.md) ou [![](./img/pdf.jpg)](Chapitres/c7_parcours_sequentiel/c7_for_string.pdf)  
    * TP [en ligne](./Chapitres/c7_parcours_sequentiel/tp_parcours_chaines.md) ou [![](./img/pdf.jpg)](Chapitres/c7_parcours_sequentiel/tp_parcours_chaines.pdf)  

---
![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-004.gif)  

__Chapitre 6 : la boucle bornée FOR__  
    * cours : cours [en ligne](./Chapitres/c6_boucle_bornee/C6_cours_for.md) ou [![](./img/pdf.jpg)](c6_boucle_bornee/C6_cours_for.pdf) ou [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2FCecGhesq%2F1_nsi_21/HEAD?labpath=Chapitres%2Fc6_boucle_bornee%2Fnb_for%2Fboucles_for.ipynb)   
    * TP [en ligne](./Chapitres/c6_boucle_bornee/tp_for/tp_for.md) ou [![](./img/pdf.jpg)](./Chapitres/c6_boucle_bornee/tp_for/tp_for.pdf)  

---
![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-005.gif)  

__Chapitre 5 : les booléens; conditions__  
    * cours : cours [en ligne](./Chapitres/c5_booléen_conditions/cours_booleens_conditions.md) ou [![](./img/pdf.jpg)](./Chapitres/c5_booléen_conditions/cours_booleens_conditions.pdf)  
    * TP [en ligne](./Chapitres/c5_booléen_conditions/tp_booleens_conditions/tp_booleens_conditions.md) ou [![](./img/pdf.jpg)](./Chapitres/c5_booléen_conditions/tp_booleens_conditions/tp_booleens_conditions.pdf)  

---
![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-006.gif)  

__Chapitre 4 : La boucle non bornée TANT QUE__  
    * cours : cours [en ligne](https://framagit.org/CecGhesq/1_nsi_21/-/blob/main/Chapitres/c4_while/c4_while_algo_2020_git.md) ou [![](./img/pdf.jpg)](https://framagit.org/CecGhesq/1_nsi_21/-/blob/main/Chapitres/c4_while/C4_while_algo_2020.pdf)  
    * TP [en ligne](https://framagit.org/CecGhesq/1_nsi_21/-/blob/main/Chapitres/c4_while/tp/tp_boucles_while.md) ou [![](./img/pdf.jpg)](https://framagit.org/CecGhesq/1_nsi_21/-/blob/main/Chapitres/c4_while/tp/tp_boucles_while_v1.pdf)  

---
![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-007.gif) 

__Chapitre 3 : LES REELS__  
    * Introduction : le problème des flottants : [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2FCecGhesq%2F1_nsi_21/HEAD?filepath=Chapitres%2Fc3_representation_reels%2Fproblemeflottants.ipynb)  
    * cours : cours [en ligne](./Chapitres/c3_representation_reels/Cours_nombres_reels_git.md) ou [![](./img/pdf.jpg)](./Chapitres/c3_representation_reels/Cours_nombres_reels.pdf)  
    * TP [en ligne](./Chapitres/c3_representation_reels/exercices.md) ou [![](./img/pdf.jpg)](./Chapitres/c3_representation_reels/exos_nombres_reels.pdf)
    
---
![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-008.gif)  

__Chapitre 2 : LES ENTIERS__

* C2_1 les bases numériques  

    * cours [ en ligne](./Chapitres/c2_representation_entiers/c2_1_bases_numeration/C2_1_les_bases.md) ou [![](./img/pdf.jpg)](./Chapitres/c2_representation_entiers/c2_1_bases_numeration/C2_1_les_bases.pdf)
    * TP [en ligne](./Chapitres/c2_representation_entiers/c2_1_bases_numeration/TP/tp1_bases.md) ou [![](./img/pdf.jpg)](./Chapitres/c2_representation_entiers/c2_1_bases_numeration/TP/tp1_bases.pdf)

* C2_2 les entiers relatifs

    * Cours [en ligne](./Chapitres/c2_representation_entiers/c2_2_cpt2/C2_2_complementadeux_git.md) 
    * TP [en ligne](./Chapitres/c2_representation_entiers/c2_2_cpt2/exos/C2_2_exercices_complement_a_deux.md) ou [![](./img/pdf.jpg)](./Chapitres/c2_representation_entiers/c2_2_cpt2/exos/C2_2_exercices_complement_a_deux.pdf)  
    
![](https://www.icone-gif.com/gif/chiffres/pixel/chiffres-gif-009.gif)  
---
__Chapitre 1 : DECOUVERTE DU PYTHON__  
* C1_1 Manipuler des nombres
    * cours [ en ligne](./Chapitres/c1_decouverte_python/manipuler_nombres/C1_1.md) ou [![](./img/pdf.jpg)](./Chapitres/c1_decouverte_python/manipuler_nombres/C1_1.pdf)
    * TP [en ligne](./Chapitres/c1_decouverte_python/manipuler_nombres/tp1_nombres.md) ou [![](./img/pdf.jpg)](./Chapitres/c1_decouverte_python/manipuler_nombres/tp1_nombres.pdf)
* C1_2 Les fonctions :
    * cours Notebook [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2FCecGhesq%2F1_nsi_21/HEAD?filepath=Chapitres%2Fc1_decouverte_python%2Fpython2_fonctions%2Fcours2_fonctions.ipynb) ou [md](./Chapitres/c1_decouverte_python/python2_fonctions/cours2_fonctions.md) ou [![](./img/pdf.jpg)](./Chapitres/c1_decouverte_python/python2_fonctions/cours2_fonctions.pdf)
    * TP  [en ligne](./Chapitres/c1_decouverte_python/python2_fonctions/tp2_fonctions/tp2_fonctions.md)  
    1. correction [cercle](./Chapitres/c1_decouverte_python/python2_fonctions/tp2_fonctions/cercle.py) et [test_module](./Chapitres/c1_decouverte_python/python2_fonctions/tp2_fonctions/test_module.py)
    2. correction [ex7](./Chapitres/c1_decouverte_python/python2_fonctions/tp2_fonctions/ex7.py)
* C1_3 Les chaines :  
    * cours [md](./Chapitres/c1_decouverte_python/python3_strings/cours3_strings.md) ou [![](./img/pdf.jpg)](./Chapitres/c1_decouverte_python/python3_strings/pdf/cours3_strings.pdf)
    * TP [md](./Chapitres/c1_decouverte_python/python3_strings/tp3_strings/tp3_strings.md)
