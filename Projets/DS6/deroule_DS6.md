# DS 6  du 23 février 2022

Le DS se déroule en deux parties : 

* une partie sur [Weblinux](http://weblinux.univ-reunion.fr) dont les résultats seront à coller grâce à des copies d'écran dans le sujet ; enregistrer régulièrement et exporter votre fichier en pdf pour le déposer dans Pronote.

* une seconde partie sur les tuples grâce à un notebook Jupyter  : dans l'ENT, choisir l'application `Jupyter`.     
![](jupy.jpg)  

Faire glisser votre fichier DS6.ipynb dans la partie gauche de l'application : il s'ouvrira dans la fenêtre principale.   
![](ds.jpg)

__Pour rendre votre exercice__ : onglet File/Download/Enregistrer dans vos Documents puis déposer dans Pronote.



Selon votre place, vous saurez si vous réalisez le sujet A ou B.

--> __Télécharger__ les fichiers dans vos Documents : 

|Sujet A|Sujet B|
|:---|:---|
|[Bash_A](DS6_OS_A.odt)|[Bash_B](DS6_OS_B.odt)|
|[Notebook Tuples_A](DS6_tuples_A-él.ipynb)|[Notebook Tuples_B](https://framagit.org/CecGhesq/1_nsi_21/-/blob/main/Projets/DS6/DS6_tuples_B-%C3%A9l.ipynb)|  

__N'OUBLIEZ PAS D'ENREGISTRER REGULIEREMENT VOTRE TRAVAIL !__
