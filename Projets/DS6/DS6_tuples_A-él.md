# DS 6_A les tuples  


Vous allez utiliser un notebook Jupyter. Chaque cellule est prévue pour que vous puissiez écrire dedans.
La cellule peut être du code Python ou du texte. 
Pour exécuter la cellule il suffit de faire __ALT + ENTER__. 

Pour la sélectionner, il faut cliquer dedans.

_Vous allez utiliser les tuples afin de créer différentes fonctions pour gérer des notes._  

1. __Créer__ ci-dessous le tuple `t1` possédant trois notes comprises entre 0 et 20 : 


```python
t1 = ................
```

2. __Compléter__  la fonction `somme(t)` ayant pour argument un tuple, réalisant la somme des valeurs contenue dans le tuple. Ne pas oublier de renseigner la docstring.


```python
def somme(t) : 
    '''renvoie la ............................
    :t : (.............)
    :return : (.................) '''
    somme = 0
    for elt in t :
        somme = .................
    return somme
```

--> Exécuter la cellule ci-dessous pour vérifier.


```python
somme(t1)
```

3. Quelle fonction Python, commune aux tuples et listes, permet de donner le nombre d'éléments d'un tuple ?

La fonction est : ................

4. __Compléter__ la fonction suivante permettant de renvoyer la moyenne arrondie à une décimale des éléments d'un tuple :


```python
def moyenne(t) : 
    '''renvoie la moyenne des éléments du tuple
    :t: (tuple) 
    :return : moyenne
    '''
    som = somme(t)
    nb_elt = ............
    return round(...............,1) # arrondit le nombre à une décimale
```

--> Exécuter la cellule ci-dessous pour vérifier.


```python
moyenne(t1)
```

5. __Ecrire__ la fonction permettant de retrouver la note maximale du tuple :


```python
def note_max(t) :
    '''renvoie la valeur maximale du tuple
     :t: (tuple) 
    :return : note_max    
    '''
    
```

--> Exécuter la cellule ci-dessous pour vérifier.


```python
note_max(t1)
```

6. Créer deux autres tuples t2 et t3 correspondant aux notes des trimestres suivants : 


```python
t2 = 
t3 = 
```

    On crée un tuple de tuples regroupant toutes les notes du trimestre : exécuter bien la cellule suivante.


```python
t_annee= (t1, t2, t3)
```

7. __Compléter__ la fonction suivante permettant de renvoyer un tuple avec les moyennes des trois trimestres : on utilisera la fonction `moyenne` précédente  dans le code.


```python
def moy_tri(t_a):
    '''renvoie un tuple avec les moyennes des trois trimestres
    : t_a : (tuple) : tuple de tuples 
    :return : tuple
    '''
    t_m = ()  # initie le tuple de retour
    for ................ : 
        t_m = t_m + (..........  ,)
    return t_m
        
```

--> Exécuter la cellule ci-dessous pour vérifier.


```python
moy_tri(t_annee)
```

__Veuillez ENREGISTRER votre travail dans vos documents PUIS le déposer dans PRONOTE.__
