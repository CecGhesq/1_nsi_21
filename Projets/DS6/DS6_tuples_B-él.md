# DS 6_B les tuples  

Vous allez utiliser un notebook Jupyter. Chaque cellule est prévue pour que vous puissiez écrire dedans.
La cellule peut être du code Python ou du texte. 
Pour exécuter la cellule il suffit de faire __ALT + ENTER__. 

Pour la sélectionner, il faut cliquer dedans.

Vous allez utiliser les tuples afin de créer différentes fonctions pour gérer des longueurs de sauts à ski sur un tremplin.  

1. __Créer__ ci-dessous le tuple `t1` possédant trois distances comprises entre 0 et 140 : 


```python
t_saut1 = ..................
```

2. __Compléter__  la fonction `somme(tup)` ayant pour argument un tuple, réalisant la somme des valeurs contenue dans le tuple. Ne pas oublier de renseigner la docstring.


```python
def somme(tup) : 
    '''renvoie .........................
    :t : (.............)
    :return : (.................) '''
    somme = 0
    for saut in tup :
        somme = .................
    return somme
```

--> Exécuter la cellule ci-dessous pour vérifier.


```python
somme(t_saut1)
```




    356



3. Quelle fonction Python, commune aux tuples et listes, permet de donner le nombre d'éléments d'un tuple ?

La fonction est : ................

4. __Compléter__ la fonction suivante permettant de renvoyer la moyenne arrondie à deux décimales des éléments d'un tuple :


```python
def moyenne(tup) : 
    '''renvoie la moyenne des éléments du tuple
    :tup: (tuple) 
    :return : moyenne
    '''
    som = somme(tup)
    nb_elt = ................
    return round(..................,2) # arrondit le nombre à 2 décimales
```

--> Exécuter la cellule ci-dessous pour vérifier.


```python
moyenne(t_saut1)
```




    118.67



5. __Ecrire__ la fonction permettant de retrouver la distance minimale parcourue par le sauteur:


```python
def distance_min(tup) :
    '''renvoie la valeur minimale du tuple
     :tup: (tuple) 
    :return : dist_min    
    '''
    
```


```python
distance_min(t_saut1)
```




    114



6. Créer deux autres tuples t_saut2 et t_saut3 correspondant aux sauts des jours suivants (mettez plusieurs valeurs, au choix, comprises entre 0 et 140) : 


```python
t_saut2 = 
t_saut3 = 
```

    On crée un tuple de tuples regroupant toutes les sauts des entrainements : exécuter bien la cellule suivante.


```python
t_e= (t_saut1, t_saut2, t_saut3)
```

7. __Compléter__ la fonction suivante permettant de renvoyer un tuple avec les moyennes des trois entrainements : on utilisera la fonction `moyenne` précédente  dans le code.


```python
def moy_tri(tup_e):
    '''renvoie un tuple avec les moyennes des trois entrainements
    : tup_e : (tuple) : tuple de tuples 
    :return : tuple
    '''
    t_moy = () # initie le tuple de retour
    for ...................... : 
        t_moy = t_moy + (........   ,)
    return t_moy
        
```

--> Exécuter la cellule ci-dessous pour vérifier.


```python
moy_tri(t_e)
```




    (118.67, 122.67, 125.75)



__Veuillez ENREGISTRER votre travail dans vos documents PUIS le déposer dans PRONOTE.__
