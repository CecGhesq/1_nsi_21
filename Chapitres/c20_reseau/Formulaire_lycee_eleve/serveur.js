/* Importation des modules*/
let express = require('express');
/*Création du serveur via le middleware express*/
let serveur = express();
/* Choix du moteur de template ejs */
serveur.set('view engine', 'ejs');

/* Utilisation de la méthode static() pour le routage des fichiers javascript et css */
serveur.use(express.static('public'));

/* Utilisation de la méthode query() pour parser la requête GET*/
serveur.use(express.query());

/* Utilisation de la méthode urlencoded() pour parser la requête POST*/
serveur.use(express.urlencoded({ extended: true })) 

/* Requête GET à la racine (localhost:3000) fonction de callback qui retourne au client le fichier index.ejs*/
serveur.get('/', (req, response) => {
	response.render('index.ejs');

});

/* Requête GET /index1.ejs depuis pages/index.ejs (localhost:3000) fonction de callback qui retourne au client le fichier index1.ejs*/
serveur.get('/index1.ejs', (req, response) => {	
	console.log(req.query);
	response.render('index1.ejs');
});

/* Requête POST /index2.ejs depuis pages/index1.ejs (localhost:3000) fonction de callback qui retourne au client le fichier index2.ejs*/
serveur.post('/index2.ejs', (req, response) => {
	console.log(req.body);
	response.render('index2.ejs');
});

/* Ecoute du serveur sur le port 3000*/
serveur.listen(3000);