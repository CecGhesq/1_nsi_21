---
title: "Chapitre 21  : Traitement des données en table "  
subtitle: "PARTIE 2 : Tri dans une table  et fusion de tables- Cours"  
papersize: a4  
geometry: margin=1.5cm  
fontsize: 12pt  
lang: fr  

---

# Tri d'une table de données

## La fonction sorted(table)

### Définition

> 📢En Python, la fonction `sorted(tableau)` permet de trier un tableau.  
Le tableau initial  n'est pas modifié comme on peut le voir ci-dessous :


```python
tableau = [51,87,47,87,12]
tableau_trie= sorted(tableau)
print("le tableau est : ",tableau , "; le tableau trié est : ", tableau_trie)
```

    le tableau est :  [51, 87, 47, 87, 12] ; le tableau trié est :  [12, 47, 51, 87, 87]
    

Remarque : ne confondez pas avec la procédure de tri des listes " List.sort() "qui va modifier la liste elle-même.

### Tri selon une clé
> 📢 La fonction `sorted` dispose d’un argument nommé `key` permettant de préciser selon quel critère une liste doit être triée.  
Cela va être très utile dans nos listes de dictionnaires comme celui utilisé dans le cours précédent. En effet _Python ne sait pas comparer deux objets qui sont des dictionnaires_. 




```python
listecommunes = [{'insee': '59005', 'nom': 'Allennes-les-Marais', 'surface': '5529290.793', 'CODE POSTAL': '59251'}, 
{'insee': '59009', 'nom': "Villeneuve-d'Ascq", 'surface': '27589625.1', 'CODE POSTAL': '59650'}, 
{'insee': '59011', 'nom': 'Annœullin', 'surface': '8964079.542', 'CODE POSTAL': '59112'}, 
{'insee': '59013', 'nom': 'Anstaing', 'surface': '2311779.151', 'CODE POSTAL': '59152'},
{'insee': '59017', 'nom': 'Armentières', 'surface': '6294040.451', 'CODE POSTAL': '59280'}]

```

```python
sorted(listecommunes)
TypeError                                 Traceback (most recent call last)
<ipython-input-4-8848f8c4b848> in <module>
----> 1 sorted(listecommunes)
TypeError: '<' not supported between instances of 'dict' and 'dict'
````

On va donc utiliser l'argument `key`. Cet argument renvoie à une fonction permettant d'obtenir la clé selon laquelle le tri va être effectué.  

Par exemple, si on souhaite trier la table selon les noms par ordre alphabétique:  

1. Pour cela il faut créer une **fonction qui va renvoyer la valeur de la clé** ’nom’ dans chaque ligne (dictionnaire).
2. Ensuite on va passer la fonction en **paramètre key**. La valeur du paramètre key devrait être une fonction qui prend un seul argument et renvoie une clef à utiliser à des fins de tri.
Cette technique est rapide car la fonction clef est appelée exactement une seule fois pour chaque enregistrement en entrée.


```python
def nom(listecommunes): # on crée la fonction qui renvoie les différentes valeurs de la clé "nom"
    return listecommunes['nom'] # on obtient les noms des différentes villes qui sont les valeurs de "nom"

tri_nom=sorted(listecommunes,key=nom) # on trie en fonction de cette clé
print (tri_nom)
```

    [{'insee': '59005', 'nom': 'Allennes-les-Marais', 'surface': '5529290.793', 'CODE POSTAL': '59251'}, {'insee': '59011', 'nom': 'Annœullin', 'surface': '8964079.542', 'CODE POSTAL': '59112'}, {'insee': '59013', 'nom': 'Anstaing', 'surface': '2311779.151', 'CODE POSTAL': '59152'}, {'insee': '59017', 'nom': 'Armentières', 'surface': '6294040.451', 'CODE POSTAL': '59280'}, {'insee': '59009', 'nom': "Villeneuve-d'Ascq", 'surface': '27589625.1', 'CODE POSTAL': '59650'}]
    

> 📢 Pour résumer, on considère que table est un tableau de dictionnaires. Alors la fonction f associée à l'argument de tri **key** de la table sera de la forme :  

```python
#fonction de tri f associée à l'argument key
def f(ligne):
    return ligne[attribut] # attribut = clé du dictionnaire

Table_triee = sorted(table, key = f)
```

Nous avions vu en TP sur les algorithmes glouton la possibilité d'utiliser les expressions dite `lambda`. L’avantage est que la fonction ainsi définie peut donc être __directement placée__ lors de l'« affectation » key= dans l’appel.  
Voici la syntaxe pour le même tri que précédemment:  

```python
tri_nom2 = sorted(listecommunes,key=lambda nom : nom['nom'] ) 
print (tri_nom2)

```

    [{'insee': '59005', 'nom': 'Allennes-les-Marais', 'surface': '5529290.793', 'CODE POSTAL': '59251'}, {'insee': '59011', 'nom': 'Annœullin', 'surface': '8964079.542', 'CODE POSTAL': '59112'}, {'insee': '59013', 'nom': 'Anstaing', 'surface': '2311779.151', 'CODE POSTAL': '59152'}, {'insee': '59017', 'nom': 'Armentières', 'surface': '6294040.451', 'CODE POSTAL': '59280'}, {'insee': '59009', 'nom': "Villeneuve-d'Ascq", 'surface': '27589625.1', 'CODE POSTAL': '59650'}]
    

### Ordre croissant et décroissant
> Un troisième argument `reverse` (un booléen) permet de préciser si on veut le résultat par ordre croissant par défaut) ou décroissant (reverse=True).



```python
tri_nom_dec=sorted(listecommunes,key=nom, reverse=True) 
print (tri_nom_dec)
```

    [{'insee': '59009', 'nom': "Villeneuve-d'Ascq", 'surface': '27589625.1', 'CODE POSTAL': '59650'}, {'insee': '59017', 'nom': 'Armentières', 'surface': '6294040.451', 'CODE POSTAL': '59280'}, {'insee': '59013', 'nom': 'Anstaing', 'surface': '2311779.151', 'CODE POSTAL': '59152'}, {'insee': '59011', 'nom': 'Annœullin', 'surface': '8964079.542', 'CODE POSTAL': '59112'}, {'insee': '59005', 'nom': 'Allennes-les-Marais', 'surface': '5529290.793', 'CODE POSTAL': '59251'}]
    

## Ordre lexicographique  

### Définition  

>📢 En mathématiques, un ordre lexicographique est un ordre que l'on définit sur les suites finies d'éléments d'un ensemble ordonné.  

Pour Python, deux *listes de nombres* sont toujours comparables via l’ordre lexicographique, même si les deux listes n’ont pas même longueur. 

Pour comparer des *chaînes*, Python utilise l’ordre lexicographique qui est lui-même basé sur l’ordre des caractères. Il se trouve que chaque caractère unicode a un numéro d’ordre (une valeur unicode) et c’est ce numéro qui est utilisé pour classer les caractères. 

Prenons un exemple : un commerce de vente de masques FFP2 souhaite remercier ses meilleurs clients par un cadeau. La propriétaire de la boutique voudrait classer les clients par points obtenus grâce aux achats. Ainsi elle va trier selon deux critères, d'abord selon le nombre de points puis, à valeur égale selon l'ordre alphabétique afin que les clients se retrouvent plus facilement dans la liste de lots. Elle réalise un tri selon l'ordre lexicographique.  
Cependant, les deux critères ne pourront être l'un en ordre croissant, l'autre en ordre décroissant :

```python
Fichier_clients = [{"nom":"Dark Vador" ,"points" :125}, 
           {"nom" :"Spiderman","points" : 68} ,
           {"nom" :"Ironman","points" : 54} , 
           {"nom" : "Kylo Ren", "points": 25}, 
           {"nom" :"Arya Stark","points" : 125},
           {"nom" :"Fantomas", "points" :54},
           {"nom" :"Sacha Pyjamasque","points": 68},
           {"nom" : "Zorro" ,"points" :68 } ,
           {"nom" :"Jason Vendreditreize", "points" : 56},
           {"nom" : "Patrick Pelloux", "points":54} ]
def points_puis_nom(x):
    return x["points"], x["nom"]

liste_a_afficher = sorted(Fichier_clients, key =points_puis_nom,)
print(liste_a_afficher)
```

    [{'nom': 'Kylo Ren', 'points': 25}, {'nom': 'Fantomas', 'points': 54}, {'nom': 'Ironman', 'points': 54}, {'nom': 'Patrick Pelloux', 'points': 54}, {'nom': 'Jason Vendreditreize', 'points': 56}, {'nom': 'Sacha Pyjamasque', 'points': 68}, {'nom': 'Spiderman', 'points': 68}, {'nom': 'Zorro', 'points': 68}, {'nom': 'Arya Stark', 'points': 125}, {'nom': 'Dark Vador', 'points': 125}]
    

**Remarques importantes** : 
* Il faudra être vigilant sur l'ordre alphabétique dans les chaînes de caractères : **une majuscule (haut de casse) est  classée avant une minuscule (bas de casse)** : la **sensibilité à la casse** (traduction de l'anglais case sensitivity) est une notion informatique signifiant que dans un contexte donné, le sens d'une chaîne de caractères (par exemple un mot, un nom de variable, un nom de fichier, un code alphanumérique etc) dépend de la casse (capitale  ou bas de casse) des lettres qu'elle contient.Les caractères a, b, etc reçoivent les valeurs consécutives 97, 98, etc.  Les caractères majuscules A, B, etc reçoivent les valeurs 65, 66, etc. Donc, un caractère majuscule est toujours classé avant un caractère minuscule. 

* Pour obtenir un tri qui tienne compte de l’ordre alphabétique en usage et en particulier des accents, des caractères spéciaux, il faut activer le module standard de localisation (qui permet de tenir compte des conventions linguistiques) : voir [Annexe 1](#annexe_1)


### Stabilité du tri en Python

Lorsque la fonction `sorted` effectue un tri à l’aide d’une clé, les éléments ayant des clés de même valeur sont placés dans la liste triée dans le même ordre que dans la liste initiale. On dit que le tri effectué en Python est un **tri stable**.

Par exemple, si notre fichier clients précédent est classé par ordre alphabétique puis qu'on trie ensuite par le nombre de points, il suffit que les clients ayant le même nombre de points restent dans leur position relative :


```python
# tri ordre alphabétique
def nom(x):
    return x["nom"]
Fichier_clients_alpha = sorted(Fichier_clients , key =nom )

# tri par lot
def points(x):
    return x["points"]

Fichier_clients_lot = sorted(Fichier_clients_alpha , key =points, reverse = True)

>>> print ("le fichier par ordre des lots est : ",Fichier_clients_lot)
le fichier par ordre des lots est :  [{'nom': 'Arya Stark', 'points': 125}, {'nom': 'Dark Vador', 'points': 125}, {'nom': 'Sacha Pyjamasque', 'points': 68}, {'nom': 'Spiderman', 'points': 68}, {'nom': 'Zorro', 'points': 68}, {'nom': 'Jason Vendreditreize', 'points': 56}, {'nom': 'Fantomas', 'points': 54}, {'nom': 'Ironman', 'points': 54}, {'nom': 'Patrick Pelloux', 'points': 54}, {'nom': 'Kylo Ren', 'points': 25}]
```

On observe ainsi que le tri s'est fait dans l'ordre alphabétique tout d'abord puis le tri en place par nombre de points permet d'afficher le fichier par nombre de points décroissant.

## Tri en place avec la méthode sort()

> 📢 La méthode `sort()` appliquée à une liste **modifie** la liste en la triant. On économise ainsi de la mémoire. On dit qu'il s'agit là d'un **tri en place**.
Par conséquent, la méthode sort s’applique essentiellement à des listes et ne s’applique pas à des conteneurs immutables ou non séquentiels. Ainsi, elle ne s’applique pas à des chaînes, des tuples et des dictionnaires.  

```python
liste = [7,8,4,5,3]
liste.sort()
>>> print(liste)
[3, 4, 5, 7, 8]
```

Dans le cas de liste de dictionnaires, on peut utiliser comme pour la fonction `sorted` les arguments `key` et `reverse`. La liste originale est alors **détruite** : 

```python
print("Voici la liste avant le tri",listecommunes)

def nom(listecommunes): # on crée la fonction qui renvoie les différentes valeurs de la clé "nom"
    return listecommunes['nom'] # on obtient les noms des différentes villes qui sont les valeurs de "nom"
listecommunes.sort(key=nom)
print()
print ("Voici la liste APRES le tri",listecommunes)


Voici la liste avant le tri [{'insee': '59005', 'nom': 'Allennes-les-Marais', 'surface': '5529290.793', 'CODE POSTAL': '59251'}, {'insee': '59009', 'nom': "Villeneuve-d'Ascq", 'surface': '27589625.1', 'CODE POSTAL': '59650'}, {'insee': '59011', 'nom': 'Annœullin', 'surface': '8964079.542', 'CODE POSTAL': '59112'}, {'insee': '59013', 'nom': 'Anstaing', 'surface': '2311779.151', 'CODE POSTAL': '59152'}, {'insee': '59017', 'nom': 'Armentières', 'surface': '6294040.451', 'CODE POSTAL': '59280'}]

Voici la liste APRES le tri [{'insee': '59005', 'nom': 'Allennes-les-Marais', 'surface': '5529290.793', 'CODE POSTAL': '59251'}, {'insee': '59011', 'nom': 'Annœullin', 'surface': '8964079.542', 'CODE POSTAL': '59112'}, {'insee': '59013', 'nom': 'Anstaing', 'surface': '2311779.151', 'CODE POSTAL': '59152'}, {'insee': '59017', 'nom': 'Armentières', 'surface': '6294040.451', 'CODE POSTAL': '59280'}, {'insee': '59009', 'nom': "Villeneuve-d'Ascq", 'surface': '27589625.1', 'CODE POSTAL': '59650'}]
```   

> **A retenir** :  
    Pour trier des données en table, on peut utiliser **les fonctions de tri fournies par Python**, à savoir **sorted** et **sort**. La première renvoie un nouveau tableau et la seconde trie en place. Ces deux fonctions assure la tabilité, c'est à dire qu'elles conservent l'ordre respectif des éléments égaux.

# Fusion de tables

Lorsqu'on travaille sur des tables de données, on peut se retrouver avec plusieurs tables qu'on aimerait rassembler afin d'effectuer des opérations de filtration , tri,etc. ...
Il existe plusieurs façons de "fusionner" des données en tables. Selon l'opération que l'on souhaite effectuer, des précautions particulières sont à prendre pour ne pas introduire d'incohérence dans les données.

## Réunion de tables
Une première opération naturelle est de réunir dans une même table les données de deux (ou plusieurs) autres tables qui ont les même structures (même attributs, c'est à dire mêmes noms des colonnes).
>📢 Afin de réunir deux tables ayant les mêmes attributs, on utilise la concaténation `+`

Afin de réunir les deux tables suivantes dans une troisième, il va falloir réaliser différentes étapes :  

![notes1](./fig/note1.png)  
![notes2](./fig/note2.png)  

1. Importer les données notes_1.csv et notes-2.csv dans les variables **table1** et **table2**.
2. fusionner les deux dans une **table 3** par l'opération table3 = table1 + table2.
3. Exporter les données de la table 3 dans un nouveau fichier CSV  : notes_3.csv
4. Vérifier le résultat



```python
# etape 1 : importer les données .csv

import csv

table1 = []
with open('./fichiers_cours/notes_1.csv', encoding='utf-8', newline='') as fichier:
    table1 = list(csv.DictReader(fichier, delimiter=';'))
table2 = []
with open('./fichiers_cours/notes_2.csv', encoding='utf-8', newline='') as fichier:
    table2 = list(csv.DictReader(fichier, delimiter=';'))
print ("voici la table 1 :",table1)
print ("voici la table 2 :",table2)

#etape 2 : réunion
table3 = table1 + table2
print ()
print("voici la table 3, réunion des tables 1 et 2 :",table3)

#etape 3 : Exportation 
with open('./fichiers_cours/notes_3.csv', 'w',encoding='utf-8', newline='') as csvfile :
    writer=csv.DictWriter(csvfile,['Nom', 'Prénom', 'NSI','Sciences Physiques', 'LLCE'])
    # pour écrire la ligne d'en-tête :
    writer.writeheader()
    # on place la table directement ; on remarquera le S de writerowS montrant la prise en compte de toutes les lignes
    writer.writerows(table3)

voici la table 1 : [{'Nom': 'von Neumann', 'Prénom': 'John', 'NSI': '18', 'Sciences Physiques': '19', 'LLCE': '20'}, {'Nom': 'Kahn', 'Prénom': 'Robert', 'NSI': '17', 'Sciences Physiques': '18', 'LLCE': '14'}, {'Nom': 'Lovelace', 'Prénom': 'Ada', 'NSI': '19', 'Sciences Physiques': '12', 'LLCE': '18'}, {'Nom': 'Flajolet', 'Prénom': 'Philippe', 'NSI': '16', 'Sciences Physiques': '13', 'LLCE': '14'}]  

voici la table 2 : [{'Nom': 'Turing', 'Prénom': 'Alan', 'NSI': '19', 'Sciences Physiques': '15', 'LLCE': '17'}, {'Nom': 'Hooper', 'Prénom': 'Grace', 'NSI': '18', 'Sciences Physiques': '17', 'LLCE': '18'}, {'Nom': 'al-Khwarizmi', 'Prénom': 'Muhammad', 'NSI': '16', 'Sciences Physiques': '14', 'LLCE': '20'}, {'Nom': 'Allen', 'Prénom': 'Frances', 'NSI': '17', 'Sciences Physiques': '17', 'LLCE': '19'}]

voici la table 3, réunion des tables 1 et 2 : [{'Nom': 'von Neumann', 'Prénom': 'John', 'NSI': '18', 'Sciences Physiques': '19', 'LLCE': '20'}, {'Nom': 'Kahn', 'Prénom': 'Robert', 'NSI': '17', 'Sciences Physiques': '18', 'LLCE': '14'}, {'Nom': 'Lovelace', 'Prénom': 'Ada', 'NSI': '19', 'Sciences Physiques': '12', 'LLCE': '18'}, {'Nom': 'Flajolet', 'Prénom': 'Philippe', 'NSI': '16', 'Sciences Physiques': '13', 'LLCE': '14'}, {'Nom': 'Turing', 'Prénom': 'Alan', 'NSI': '19', 'Sciences Physiques': '15', 'LLCE': '17'}, {'Nom': 'Hooper', 'Prénom': 'Grace', 'NSI': '18', 'Sciences Physiques': '17', 'LLCE': '18'}, {'Nom': 'al-Khwarizmi', 'Prénom': 'Muhammad', 'NSI': '16', 'Sciences Physiques': '14', 'LLCE': '20'}, {'Nom': 'Allen', 'Prénom': 'Frances', 'NSI': '17', 'Sciences Physiques': '17', 'LLCE': '19'}]
```


## Opération de jointure
On peut aller plus loin avec des tables ayant des attributs différents, mais **au moins un attribut commun**. 
Soit le fichier   **Travaux4.csv** qu'on va ouvrir dans une **table4**  que l'on va joindre à la table 1 ( qui ne contient que 4 personnalités):  

![notes4](./fig/note4.png) fusionné avec ![notes1](./fig/note1.png)

1. **Importer les données** de notes1.csv dans les variables **table1** et Travaux4.csv dans **table4**
2. **Créer une fonction** fusion(tableA,tableB) qui va créer un nouveau dictionnaire représentant la ligne de la table fusionnée recherchée.

On choisit ici de ne conserver que la note de NSI dans le tableau final!


```python
# Etape 1
import csv

table1 = []
with open('./fichiers_cours/notes_1.csv', encoding='utf-8', newline='') as fichier:
    table1 = list(csv.DictReader(fichier, delimiter=';')) 


table4 = []
with open('./fichiers_cours/Travaux4.csv', encoding='utf-8', newline='') as fichier:
    table4 = list(csv.DictReader(fichier, delimiter=';'))

# Etape 2 
def fusion(ligneA,ligneB) :
    ''' fusion de deux tables
    :entrée ligne A la table 1
    :entrée ligne B la table 4
    :sortie le dictionnaire fusionné
    '''
    return {'Nom' : ligneA['Nom'],'Prénom' : ligneA['Prénom'],'NSI' : ligneA['NSI'],'domaine de recherche': ligneB['domaine de recherche']}
```

3. on va créer **la jointure** de deux façons : 
    * par une double boucle
    * par une double compréhension qui s'apparente en fait plus au fonctionnement des requêtes SQL qui seront abordées en terminale


```python
# Etape 3 a : double boucle
jointure_a = []
for ligneA in table1 :
    for ligneB in table4:
        if ligneA['Nom']==ligneB['Nom']:
            jointure_a.append(fusion(ligneA,ligneB))
```


```python
# Etape 3 b : double compréhension
jointure_b=[fusion(ligneA,ligneB) for ligneA in table1   for ligneB in table4    if ligneA["Nom"]==ligneB["Nom"]]
```

4. **Exporter les données** de la jointure dans un nouveau fichier CSV : jointure1_4.csv :


```python
# Etape 4
with open('./fichiers_cours/jointure1_4.csv', 'w',encoding='utf-8', newline='') as csvfile :
    writer=csv.DictWriter(csvfile,['Nom', 'Prénom', 'NSI','domaine de recherche'])
    # pour écrire la ligne d'en-tête :
    writer.writeheader()
    # on place la table directement ; on remarquera le S de writerowS montrant la prise en compte de toutes les lignes
    writer.writerows(jointure_a)
```

Lorsqu'on importe le fichier dans un tableur, on retrouve bien la fusion des deux tableaux :
![fusion](./fig/fusion.png)


> **A retenir**  
    La **fusion** de tables de données peut se faire de plusieurs façons. Si les tables ont exactement **les mêmes attributs (colonnes)**, alors on peut concaténer ces tables pour obtenir leur **réunion**. Si elles ont **au moins un attribut commun**, on peut effectuer une opération de **jointure** qui mettra en relation les données égales selon ces colonnes communes.

sources:
* Le petit Python Aide mémoire pour Python 3 Richard Gomez Ellipses
* NSI 30 leçons T Balabonski S Conchon JC Filliâtre K Nguyen
* www.math93.com / J. Courtois, F. Duffaud
* http://pascal.ortiz.free.fr/contents/python/tri/tri.html

\newpage
# annexe_1 
exemple d'utilisation du module standard de localisation


```python
import locale #importation du module standard de localisation
locale.setlocale(locale.LC_ALL, "") #activation de la locale

L = ["Prune", "prune", "levées", "lève-glaces",
     "lèvent", "leçon", "Lecteur"]

#pour que le tri tienne compte des contraintes linguistiques, il faut utiliser la clé strxfrm du module locale
print(sorted(L, key=locale.strxfrm)) 
```

    ['leçon', 'Lecteur', 'levées', 'lève-glaces', 'lèvent', 'prune', 'Prune']
    

On remarque que cette fois ce module n'est pas sensible à la casse puisque "Prune" est placé après "prune"
