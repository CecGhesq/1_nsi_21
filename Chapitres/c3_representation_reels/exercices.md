---
title: "Chapitre 3 : Exercices sur les nombres réels"
subtitle: "exercices "
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---


# Exercice n°1  
  
Convertir en binaire les nombres réels suivants avec une précision de 8 chiffres après la virgule en base 2

- 11/15 (On calcule la division et on prend trois chiffres après la virgule.)

- 101/8

- 12.458

# Exercice n°2 

- On code en format simple précision de la norme IEEE-754 soit sur 32 bits.
Déterminer la mantisse du réel 4.350 (On prendra une précision de 8 chiffres après la virgule en base 2)  

- On a la représentation suivante  0  10000110  01101100011000111101011  Norme IEEE-754 simple précision (signe, exposant, mantisse)   
Quelle est la valeur du réel en base 10?

# Exercice n°3 

- Donner la représentation de 0.1<sub>(10)</sub> en binaire.

- Donner la représentation de 0.2<sub>(10)</sub> en binaire.

- Calculer la somme des deux réels en base 10 et en binaire.
Que pouvez-vous en conclure?

Sachant que les ordinateurs utilisent le binaire, expliquez pourquoi on ne peut comparer deux réels?


