---
title: "Chapitre 3 : Représentation des nombres réels "
papersize: a4
geometry: margin=1.5cm
fontsize: 11pt
lang: fr
---  
  
  

# Introduction  

- Vous savez maintenant comment sont représentés des entiers naturels et des entiers relatifs. Vous avez même réalisé des calculs de base avec ces entiers.

- La représentation des nombres réels est plus délicate à mettre en oeuvre et à comprendre. En effet, les réels peuvent être des décimaux ( 0.125  ;  3.0  ; 0.345 ), des nombres rationnels ( 1/3 ; 11/15 ...) ou des irrationnels comme Pi ...

- Dans notre cours, nous allons voir comment sont représentés ces nombres réels, les approximations qui sont faites pour certains nombres réels et surtout les dangers de comparer des réels entre eux.

- Nous terminerons par la norme IEEE - 754 qui formalise la représentation des nombres réels en informatique.

# Représentation en binaire des nombres réels

## Cas des nombres dyadiques

Par analogie avec les nombre décimaux, il existe des nombres flottants en binaire.

> 📢 On appelle __nombre dyadique__ tout nombre pouvant s'écrire sous la forme $`\dfrac{x}{2^{n}}`$, avec $`x \in \mathbb{Z}`$ et $`n \in \mathbb{N}`$.</div>

On souhaite représenter en binaire la fraction :  $`\dfrac{11}{16}`$ ce nombre est  __dyadique__ car    $`\dfrac{11}{16} = \dfrac{11}{2^{4}}`$  

Dès lors on peut comme pour un nombre décimal procéder à un développement de la fraction  :  

* le numérateur peut s'exprimer ainsi : $`11=8+2+1=2^{3}+2^{1}+2^{0}`$  

* d'où : $`\dfrac{11}{16} = \dfrac{2^{3} +2^{1}+2^{0}}{2^{4}} = 2^{-1} +2^{-3}+2^{-4}`$  

Comme pour un nombre décimal, on peut exprimer un nombre flottant en binaire :

$`\dfrac{11}{16} = \underbrace{\boxed 0}_{\text{partie  entière}}\times2^{0} +\underbrace{\boxed 1\times2^{-1}+\boxed 0\times2^{-2}+\boxed 1\times2^{-3}+\boxed 1\times2^{-4}}_{\text{partie  fractionnaire}}= \bf(0.1011)_{2}`$ 

On voit qu'il est facile dans notre exemple de convertir un dyadique en binaire sans perte d'information. Comment cela se passe-t-il dans les autres cas ?   

## Cas général  

Prenons l'exemple du nombre __48,195__ comportant une partie entière __48__ et une partie fractionnaire __0,195__

Nous avons déjà vu comment convertir un entier naturel signé en binaire (_sur 8 bits par exemple_) :  $`48 = (0011 0000)_{2}`$   
Nous allons voir comment dans un cas quelconque représenter la __partie fractionnaire__.  
  
> __📌Méthode:__   : Tant que la précision souhaitée n'a pas été atteinte :
> 
> * On multiplie par deux la partie fractionnaire    
>    * si le résultat obtenu est supérieur ou égal à 1, on conserve un __1__
>    * si le résultat est inférieur à 1, on conserve un __0__
> * La nouvelle partie fractionnaire correspond à la partie fractionnaire (et uniquement elle) du résultat.
>
> __La succession de 1 et de 0 conservés correspondra à la représentation binaire de la partie fractionnaire__  


__📝Application__ :  
Représentation de  la partie fractionnaire de 48,195 avec une précision de 10 chiffres après la virgule.

|étapes &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|test de la valeur |chiffre conservé|Puissance de 2 correspondante|  
|-----|:------:|:---:|----|  
|$`0.195\times2=0.390`$|<1|__0__|$`2^{-1}`$|  
|$`0.390\times2=0.780`$|<1|__0__|$`2^{-2}`$|  
|$`0.780\times2=1.560`$|>1|__1__|$`2^{-3}`$|  
|$`0.560\times2=1.120`$|>1|__1__|$`2^{-4}`$|  
|$`0.120\times2=0.240`$|<1|__0__|$`2^{-5}`$|  
|$`0.240\times2=0.480`$|<1|__0__|$`2^{-6}`$|  
|$`0.480\times2=0.960`$|<1|__0__|$`2^{-7}`$|  
|$`0.960\times2=1.920`$|>1|__1__|$`2^{-8}`$|  
|$`0.920\times2=1.840`$|>1|__1__|$`2^{-9}`$|  
|$`0.840\times2=1.680`$|>1|__1__|$`2^{-10}`$|  

On peut bien évidemment continuer le processus...

Ainsi on peut en déduire qu'avec 10 chiffres de précisions 0.195 est décomposé ainsi :  
$`0\times2^{-1}+0\times2^{-2}+1\times2^{-3}+1\times2^{-4}+ 0\times2^{-5}+0\times2^{-6}+0\times2^{-7}+1\times2^{-8}+1\times2^{-9}+1\times2^{-10}`$  soit $`\bf0.194`$  !
  
En faisant le calcul précédent on arrive seulement à une **approximation** de la partie fractionnaire qu'on souhaite représenter et __ce sera le cas pour la plupart des nombres flottants__

Ainsi en machine avec une précision de 10 chiffres :
$`0,195 \text{ sera représenté par }  (0.0011000111)_{2}`$

Et donc avec 8 bits pour la partie entière le nombre 48,195 sera représenté ainsi : $`(\underbrace{00110000}_{\text{partie entière}}.\underbrace{0011000111}_{\text{partie fractionnaire}})_{2}`$

Conclusion : Pour convertir un nombre réel de la base 10 à la base 2, il faut séparer partie entière et partie décimale puis appliquer les méthodes pour les entiers naturels et celle de ce parapraphe pour la partie décimale.  

**Attention, comme nous l'avons vu, nous approximons la conversion de la partie décimale dans certains cas. Il est donc impératif en programmation de ne pas comparer des réels entre eux. Le résultat sera alors aléatoire.**

# Représentation des nombres réels  : exemple de la norme IEEE-754

## L'écriture scientifique : rappel  

L'encodage des nombres flottants est inspiré de l'écriture des nombres décimaux qui se compose d'un `signe` (+ ou -), d'un nombre décimal m, appelé `mantisse` compris dans l'intervalle [1,10[ (1 inclus et 10 exclu) et d'un entier relatif n appelé `exposant`.  
Ainsi , avec cette notation :  
2341 = +$`2,341\times 10^{3}`$  
0,00456 = + $`4,56\times 10^{-3}`$   
-127854 = - $`1,27854\times 10^{5}`$ 

> 📢 L'écriture scientifique d'un nombre décimal est ainsi de la forme  $`\pm m\times 10^{n}`$ 

##  La norme IEEE - 754 : écriture des nombres *flottants*  

La norme IEEE - 754 la plus couramment utilisée pour les ordinateurs.  
Cette norme fixe pour la taille d'un mot **le nombre de bits réservés** :  

- Au signe s (0 si + ; 1 si -)

- A l'exposant n

- A la mantisse  m

> 📢La représentation d'un nombre flottant est ainsi similaire à celle de l'écriture scientifique : $`(-1)^{s}m\times 2^{n-d}`$ 

*d : décalage qui dépendra du type de précision ( 32 ou 64 bits)*  

Pour le stockage des nombres réels, la taille des mots est de 32 bits (simple précision) ou 64 bits (double précision).    
  
![format simple précision](images/formatsimple.png)
  
![format double précision](images/formatdouble.png)  
  

## Comment calculer l'exposant et la mantisse ?  
  
* Afin de représenter des exposants positifs et négatifs, la norme IEEE-754 n'utilise pas l'encodage de complément à deux des entiers relatifs mais une technque qui consiste **à stocker l'exposant de manière décalée sous la forme d'un nombre non signé.**
Ainsi l'exposant décalé n est un entier sur 8 bits compris entre 0 et 255. Pour le format 32 bits, l'exposant est décalé avec **d = 127** :   $`-127< exposant < 128`$ 

*remarque : les valeurs 0 et 255 sont réservées pour représenter des nombres particuliers donc en réalité les valeurs extremales sont -126 et 127*

* La mantisse est toujours de la forme 1,xx.....xx donc dans l'intervalle [1,2[ . Ainsi pour gagner un bit de précision, les 23 bits dddiés à la mantisse sont uniquement utilisés pour représenter les chiffres **après** la virgule (la fraction).
  > 📢 si les 23 bits dédiés à la mantisse sont $`b_{1} b_{2} b_{3} .......b_{23}`$ , alors la mantisse représente le nombre :  
 $`1 + b_{1}\times 2^{-1}+ b_{2}\times 2^{-2}+ b_{3}\times 2^{-3} .......+ b_{23}\times 2^{-23}`$ 

---
 __📌Méthode:__  Nous allons reprendre l'exemple 48.195. (Ici on prendra donc 0 pour le signe puisque le réel est positif)
 
- **Dans un premier temps, il faut convertir le nombre réel de base 10 en base 2**.  
Nous avions trouvé: $`(48,195)_{10}= (0011 0000.0011 000111)_{2}`$   

- **Ensuite il faut utiliser la notation scientifique et supprimer les zéros à gauche qui ne servent pas pour la mantisse.** Attention, la puissance ici est une puissance de 2. 
Ainsi, $`(00110000.0011000111)_{2} = (1.100000011000111 \times 2^{5})_{2}`$

De la mantisse on ne conserve que la fraction :  

**100000011000111**

- **Pour l'exposant, le bit de poids fort est réservé pour le signe de l'exposant**. Donc pour éviter d'avoir une notation trop complexe, on procède au  décalage de l'exposant c'est à dire qu'on ajoute une valeur à l'exposant en utilisant la formule suivante:  
**valeur exposant décalé= valeur exposant + $`2^{n-1}`$ -1**  
 où n représente le nombre de bits réservés à l'exposant.

Par exemple pour le format simple précision avec la valeur 5, on trouve:  
n = 8 (simple précision) et valeur exposant = 5 en base 10  
valeur exposant décalé = 5 + $`2^{7}`$ - 1 = 5 + 127 = 132 soit 1000 0100

Si on finalise le calcul, en simple précision on trouve:  
 
**0 10000100 10000001100011100000000**

---

Suivons un autre exemple dans l'autre sens :
Soit le mot de 32 bits suivant :  

| signe | exposant |         fraction        |
|:-----:|:--------:|:-----------------------:|
|   1   | 10000110 | 10101101100000000000000 |  

* le signe : $`-1^{1} = -1`$
* l'exposant : $`2^{7}+2^{3}+2^{2}127`$ = (128 + 4 + 2 ) - 127 = 134 - 127 = 7
* la mantisse :$`1+2^{-1}+2^{-3}+2^{-5}+2^{-6}+2^{-8}+2^{-9}`$  = 1,677734375 soit au final le nombre suivant :  

**-1,677734375 x 2 <sup> 7 </sup> = -214,75**
  
---  

Pour résumer les deux encodages :  

|         	| exposant (e) 	| fraction (f) 	|      valeur                                  |
|:-------:	|:------------:	|:------------:	|:-----------------------------------------:   |
| 32 bits 	|    8 bits    	|    23 bits   	| $`(-1)^{s}\times 1,f\times 2^{(e-127)}`$  |
| 64 bits 	|    11 bits   	|    52 bits   	| $`(-1)^{s}\times 1,f\times 2^{(e-1023)}`$ |


# __Une même représentation : plusieurs types de données__

On l'a vu,  la manière de coder un nombre peut être très différente en fonction des conventions utilisés:  

* codage des entiers naturels sur n bits ou de manière arbitraire (comme en Python)    
* codage des entiers signés sur n bits grâce au complément à deux
* codage des réels

Par exemple la représentation sur 8 bits suivante $`(11010111)_{2}`$ correspond :  

* soit à  $`215`$
* soit à  $`-41`$ 

Ou encore la représentation sur 32 bits $`(0 01010111 01011010001101101101100)_{2}`$ peut correspondre à  :

* $`732765036`$ si c'est un entier codé en complément à deux
* $`1.23e-12`$ si c'est un réel en format simple précision

Certains langages de programmation contrairement à Python nécessitent de spécifier le type exact d'une variable numérique.

A titre d'exemple quelques types représentant tous des entiers

|type | langage  | intervalles de nombres correspondants |  
|:---:|:---:|:---:|  
| __int__ | Python  | pas de limite supérieure|   
| __int__ | Java  | entre $`-2^{31}`$ et $`2^{31} -1`$ |   
| __long__ | Java  | entre $`-2^{63}`$ et $`2^{63} -1`$|   
| __sign char__ | C  | entre $`-2^{7} + 1`$ et $`2^{7} -1`$|    
| __int__ | C  | entre $`-2^{15}`$ et $`2^{15} -1`$|   

---  
  
**Anecdote:**  
Le 25 février 1991, durant la première guerre du Golfe, une batterie de missile basée à Dharan, en Arabie Saoudite, échoua à suivre et intercepter un missile Scud irakien en approche. Le missile Scud frappa un barraquement américain, tuant 28 soldats et blessa environ 100 autres personnes. Le rapport du commandement général, GAO/OMTEC-92-26, détailla l'échec de la manière suivante : un problème logiciel a conduit à une défaillance du système à Dharan, Arabie Saoudite. Il est apparu que la cause était un mauvais calcul du temps écoulé depuis le démarrage du missile dû à des erreurs d'arrondies numériques.

Plus spécifiquement, le temps qui était mesuré en dixième de secondes par l'horloge interne, était ensuite multiplié par la valeur 10 pour avoir le résultat en secondes. Ce calcul était réalisé sur un registre 24 bits en arithmétique fixe. La valeur 1/10, qui n'a pas de développement binaire fini, était tronquée à la 24ième décimale après la virgule. Cette petite erreur d'arrondi, une fois multipliée par un plus grand nombre lors de la conversion en seconde, conduit à un erreur d'arrondi plus significative. En effet, la batterie de missile Patriot était allumé depuis 100 heures, et un calcul rapide montre que l'erreur d'arrondi amplifiée était de l'ordre de 0.34 secondes.

En effet le nombre 1/10 est égal à 1/24 +1/25 +1/28 +1/29 +1/212 +1/213 +... En d'autres mot, l'expansion en binaire de 1/10 est 0.0001100110011001100110011001100... Comme le registre dans lequel le missile Patriot stocke la valeur n'est que de 24 bits la valeur stockée est 0.00011001100110011001100. Cette valeur introduit donc une erreur en binaire d'une valeur de 0.0000000000000000000000011001100... soit environ 0.000000095 en décimale. Si l'on multiplie par le nombre de dixièmes de seconde écoulés en 100 heures on obtient 0.000000095×100×60×60×10=0.34
