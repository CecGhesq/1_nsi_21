from math import pi

def perimetre(rayon) :
    """renvoie la valeur du périmètre
    d'un cercle
    """
    return 2*pi*rayon

def aire(rayon) :
    """renvoie la valeur de l'aire
    d'un cercle
    """
    return pi * rayon **2
