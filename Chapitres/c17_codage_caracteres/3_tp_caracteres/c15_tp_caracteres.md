---
title: "Chapitre 17 : Représentations des caractères en machine "
subtitle: " TP "
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

# Exercice 1 : QCM  &#x1F3C6;
_(Question 1 extraite de QCM sujet 0 prime, Questions 2,4 et 5 extraites de formation DIU Lille[^1] )_


__1.__  Quelle est l'affirmation vraie concernant le codage UTF-8 des caractères ?  

* [ ] Le codage UTF-8 est sur 1 à 4 octets 
* [ ] Le codage UTF-8 est sur 8 bits 
* [ ] Le codage UTF-8 est sur 8 octets 
* [ ] Le codage UTF-8 est sur 7 bits


__2.__ Lequel de ces noms ne correspond pas à un système d'encodages de texte ?  

* [ ] FAT-32
* [ ] UTF-8
* [ ] Latin-1
* [ ] UTF-16


__3.__  Quelle affirmation est vraie concernant la suite de bits `10001001` ?  

* [ ] Elle représente l'entier naturel 136 exprimé sur 8 bits
* [ ] Elle représente le caractère `A` en ASCII
* [ ] Elle représente -119 codé en complément à deux sur 8 bits
* [ ] Elle est équivalente à la représentation `FF` en hexadécimal


__4.__ Sur combien d'octets a été codée la chaîne de caractères suivante en utf-8 ?  
_égalités_

* [ ] 10
* [ ] 7
* [ ] 16
* [ ] 1

__5.__ D'après l'extrait de de table la table ASCII ci-dessous, à quel mot correspond ce code binaire :  `010000100100000101000011` ?  

| Dec | Oct | Hex | Caractère |
|-----|-----|-----|-----------|
| 065 | 101 |  41 |     A     |
| 066 | 102 |  42 |     B     |
| 067 | 103 |  43 |     C     |
| 068 | 104 |  44 |     D     |
| 069 | 105 |  45 |     E     |
| 070 | 106 |  46 |     F     |
| 071 | 107 |  47 |     G     |
| 072 | 110 |  48 |     H     |
| 073 | 111 |  49 |     I     |


* [ ] BAC
* [ ] ABC
* [ ] CAB
* [ ] ACB


# Exercice 2  : Unicode et Python &#x1F3C6;&#x1F3C6;

__1)__ Consultons l’aide sur la fonction `chr`

```python
>>> help (chr)
Help on built-in function chr in module builtins:

chr(...)
    chr(i) -> Unicode character

    Return a Unicode string of one character with ordinal i; 0 <= i <= 0x10ffff.
```

L’aide nous apprend que cette fonction renvoie une chaîne de un caractère dont le numéro (ordinal) Unicode passé en paramètre est compris entre 0 et 0x10ffff.

Par exemple la caractère de point de code __U+262E__ peut être affiché de deux manières:

```python

>>> chr (0x262e)  # soit en utilisant la notation hexadécimale
'☮'

>>> chr (9774) # soit en utilisant la notation décimale équivalente
'☮'
```

__Afficher ainsi le caractère `Შ` de point de code U+1CA8 : vous trouverez d'abord la valeur décimale équivalente__

__2)__  L'espace Unicode est divisé en 17 zones de 65 536 points de codes. Ces zones sont appelées plans eux même divisés en sous-ensembles. Vous pouvez les retrouver à l’adresse suivante :        http://www.unicodetables.com/

Le sous-ensemble __Cyrillic__ est par exemple situé entre les points de code `U+0400` et `U+04FF`

Concevez un script en Python permettant d’afficher l’intégralité de ces symboles comme ceci

![Cyrillic](./fig/cyrillic.jpg)

__Sauvegardez votre script sous le nom `script_cyrillic.py`__


__3)__ Recherchez les codes des sous-ensembles allant de __Basic Latin__ à __Latin Extended-B__.

Construisez une liste Python nommée `unicode` contenant des triplets de la forme `(nom_du_sous_ensemble, point_de_code_min, point_de_code_max)` pour ses sous-ensembles.

avec  :
* _nom_du_sous_ensemble_ : une chaine de caractère
* _point_de_code_min_   / _point_de_code_max_  : des valeurs décimales

Exemple :

```python
unicode = [('Basic Latin', 0, 127), (.., .., ..), ..]
```

🥇 Définissez enfin une procédure `affichage_unicode` qui acceptera un paramètre : _nom_du_sous_ensemble_ et qui affichera les caractères correspondant en utilisant la variable `unicode`.

![Cyrillic](./fig/affichage_unicode.jpg)

_Remarque_ : Parfois le caractère obtenu par la fonction chr se présente sous forme d’un carré : cela se produit lorsque le dispositif qui produit l’affichage ne connaît pas le glyphe associé au caractère. Un glyphe est une représentation graphique d’un signe typographique.

__AIDE__ On pourra s'aider d'une fonction annexe de recherche de l'indice auquel se trouve le bon triplet dans la liste

__Sauvegardez votre script sous le nom `script_unicode.py`__  en n'oubliant pas de réaliser une __docstring complète__ pour chacune de vos fonctions (les tests ne sont pas demandés ici)


[^1]: https://gitlab-fil.univ-lille.fr/diu-eil-lil/portail/-/raw/master/qcm/qcm.md
