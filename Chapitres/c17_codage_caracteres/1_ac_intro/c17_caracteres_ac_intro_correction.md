---
title: "Chapitre 17 : Représentation des caractères en machine "
subtitle: " Activité : codage des caractères -- CORRECTION "
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

#	Le Code ASCII

__1)__	Combien de bits sont utilisés pour encoder un caractère en ASCII ?  __7__ 
__2)__	Combien de caractères sont ainsi encodables ?  $`2^{7}=128`$


__3)__	Décoder le message suivant :

```
01001100 01100101 00100000 01100010 01101001 01101110 01100001 01101001 01110010 01100101 00101100 00100000 01100011 00100111 01100101 01110011 01110100 00100000 01101001 01101110 01101000 01110101 01101101 01100001 01101001 01101110 00101110
```

__On peut utiliser le site__ : https://www.asciitohex.com

```
Le binaire, c'est inhumain.
```

__4)__	Pour plus de facilité, le codage des caractères peut être donné en base 10 (décimal) ou en base 16 (hexadécimal).

__a-__	Retrouver (et justifier) par le calcul le codage décimal du caractère `@`  

$`(1000000)_{2}= 1×2^{6}= 64`$

__b-__	Quel caractère correspond au codage hexadécimal `3D`? (Justifier par le calcul)

$`(3D)_{16} = 3×16^{1}  + 13 ×16^{0} = 48+13= 61  = (111101)_{2}`$ sur 6 bits donc $`(0111101)_{2}`$  sur 7 bits 

caractère : `=`


__6)__	Trouver à l’aide de l’Explorateur de votre système d'exploitation (clic droit Propriétés sous Windows) la taille exacte occupée (en octets) par ce fichier.

__31 octets car 31 caractères__ (_ne pas oublier de compter les espaces_)

__7)__	Rajoutez un saut de ligne juste avant le mot « peut » 
  
__nouvelle taille : 33 octets__

__8)__ Dans le menu Affichage de Notepad++ sélectionnez : Symboles spéciaux > Afficher tous les caractères 

__a-__	Deux caractères de contrôle s’affichent lesquels ?  

__CRLF__

__b-__	Quels sont leurs codes décimaux ?

__CR: 13__   et __LF : 10__

__9)__ Recherchez l'origine du sigle CRLF et la manière dont sont codés les sauts de ligne sous 3 types de systèmes d'Exploitation (Windows, MacOS, Linux) 

__Le sigle CRLF provient de la juxtaposition du sigle de Carriage Return (retour chariot) et de Line Feed (saut de ligne) utilisés sur les machines à écrire__

__Le CRLF est surtout utilisé par les logiciels sous Windows, mais d'autres systèmes d'exploitation le reconnaissent. Cependant, certains logiciels ont des standards qui les empêchent de reconnaître le CRLF. Il existe des programmes qui convertissent le CRLF en un autre caractère de contrôle équivalent. Unix, par exemple, n'utilise que le caractère LF pour une nouvelle ligne, alors que Mac OS avant sa version 10 (version basée en partie sur BSD, un Unix) n'utilisait que le CR.  (voir Wikipedia)__




# Les extensions de l'ASCII

## La norme ISO-8859-1


__10)__ Combien de caractères supplémentaires peuvent ainsi être ajoutés ?

__128 supplémentaires portant le total à 256__


La norme __ISO-8859-1__ est aussi appelée __Latin-1__. Voici la table d'encodage correspondante:

Dans cette table les valeurs sont exprimées en hexadécimal (on lit d'abord l'en-tête de la ligne puis celle de la colonne). Par exemple le caractère `J` est codé $`4A_{16}`$

__11)__ Est-ce le même code en ASCII ?  __Oui__

__12)__ La table ASCII est-elle compatible avec la table Latin-1 ? (le vérifier pour plusieurs caractères en donnant des exemples)  __Oui les 32 premiers caractères ne sont pas utilisés en Latin-1 mais tous les autres de la table ASCII se retrouvent avec le même code en Latin-1. Au delà de 127 :ce sont des caratères n'existant pas en ASCII__

__13)__ Quels caractères très utilisés en français sont rajoutés par cet encodage ?  

__`é` `è` `à` par exemple__

__14)__ On s'intéresse plus particuliérement au caractère spécial `NBSP`.  

__a-__ Quel est son encodage décimal ?  

 $`A0_{16}= 160`$

__b-__ Cherchez son utilité en donnant un exemple.

__Une espace insécable est un signe typographique numérique consistant en une espace que l'on intercale entre deux mots (ou un mot et une ponctuation) qui ne doivent pas être séparés en fin de ligne. On les utilise en général pour respecter la typographie un espace avant ! ? ou après , par exemple en français__


## Encodage par défaut sous Windows


__15)__ Cherchez le caractère `=` dans le tableau ci-dessus.

__a-__ Retrouvez sa valeur en décimal :  __61__


__b-__ Est-ce le même code en ASCII ?  __oui__

__c-__ Vérifiez pour d'autres caractères. Ces normes d'encodage sont-elles compatibles ?  

__Oui 127 premiers caractères sont identiques à l'ASCII. Au delà de 127 : ce sont des caractères n'existant pas en ASCII__

__d-__ Et avec le Latin-1 ? Donner des exemples le justifiant et expliquant les différences.

__Oui car entre les positions hedadécimales 80 et 9F aucun caractère n'exite en Latin-1. Au delà ce sont les mêmes__


__16)__ Essayez dans un traitement de texte d'insérer par la méthode précédemment décrite le caractère `Æ`. Quel combinaison de touches faut-il utiliser ? 
__Remarques__ :

* sur Windows :  il faut maintenir la touche_ Alt_ puis taper le code avec **le pavé numérique** ; si vous n'en avez pas sur un ordinateur portable il sera peut être nécessaire d'associer _Alt+Fn+code numérique_.
  
* sur Linux : la touche _compose_ doit être utilisée ; le plus souvent elle est associée à _Alt_ ou _Win_ mais peut être aussi choisie par l'utilisateur
* sur Mac-OS : Touche Option (Alt)

__$`C6_{16}= 12 \times 16^{1} + 6 \times 16^{0} = 198`$   combinaison ALT + 0198__

## Compatibilité des normes ISO-8859-n ?

Ouvrez le fichier [`index1.html`](./files/index1.html) avec Firefox et visualisez le code source.

__17)__ Quel encodage a été utilisé pour ce fichier ? Précisez où se situe l'information dans le code HTML

__ISO-8859-1__

__18)__ Modifiez l'encodage du navigateur `Affichage/Encodage` du texte et choisissez __Europe centrale (ISO)__.  
__a-__ Qu'observez vous ?    

__Certains caractères ont changé__

__b-__ Recherchez à quelle norme ISO-8859 cet encodage correspond ?  

__Il s'agit de la norme ISO-8859-2 ou Latin-2 ou européen central__

__c-__ Les normes ISO-8859 sont-elles compatibles entre-elles ? Donner plusieurs éléments de réponse.(Il faudra notamment comparer les tables d'encodages)  

__En comparant les tables on se rend compte que pour un même code hexadécimal donc un même octet, le caractère encodé n'est pas le même. Ces normes ne sont pas compatibles entre-elles__

|code hexadécimal| caractére encodé en Latin-1|caractére encodé en Latin-2|
|:---:|:---:|:---:|
|__BC__|`¼`|`ź`|
|__BD__|`½`|`˝`|


# Vers l’Unicode

__19)__	Ouvrez LibreOffice Writer (_ou un Traitement de texte équivalent_). 

__a-__ Dans le menu `Insertion> Caractères spéciaux`, trouvez le point de code du symbole __♫__ placé dans le sous-ensemble Symboles divers  
__b-__ Retrouvez le code décimal équivalent (En justifiant) 

__U+266B__ donc : $`266B_{16}= 2 \times 16^{3} + 6 \times 16^{2} + 6 \times 16^{1} + 11 \times 16^{0} = 9835`$

__c-__ Ce code décimal existe-t-il dans la norme ISO-8859? Justifier

__Non au maximum 255__

__20)__ Toujours en utilisant le même logiciel trouver les caractères correspondant au sous-ensemble Latin-1.

__a-__ Donnez les points de code du premier et du dernier caractère de ce sous ensemble (Ils sont placés dans l'ordre : lecture de gauche à droite et de haut en bas)  

__Entre __U+0000 et U+00FF__

__b-__ Les valeurs décimales sont-elles compatibles avec celles de la norme ISO-8859-1 ?

__Oui entre 0 et 255__

Unicode se contente de recenser, nommer les caractères et leur attribuer un numéro. Mais il ne dit pas comment ils doivent être codés en informatique.

Plusieurs codages des caractères Unicode existent UTF-32, UTF-16 et le plus célébre UTF-8

Dans __UTF-8__  chaque caractère peut être codé sur 8, 16, 24 ou 32 bits 

__21)__ Combien d'octets peuvent être utilisés par un seul caractère en __UTF-8__ ?

__1, 2, 3 ou 4__

__22)__ On lit dans une table __UTF-8__ que le caractère `é` est codé sur deux octets par les valeurs hexadécimales __C3 A9__.
Expliquez alors l'image suivante en utilisant des tables d'encodage précédentes . 

__Si le texte est interprété en Latin-1 : la séquence C3 A9 sera traduite par 2 caratères car en Latin-1 chaque caractère est codé sur un octet: toujours.__
__D'après la table ces caractères seront : `Ã`  et `©`__

__23)__ Le caractère `€` sur 3 octets par les valeurs hexadécimales __E2 82 AC__.

Soit un fichier contenant le texte `"J'écris € en UTF-8"` codé en UTF-8. Quel sera le texte affiché si un logiciel décode ce texte en supposant que le codage utilisé est __Latin-1__ ?

__Les caractères ASCII seront décodés correctement. Le caractère 'é' codé 'C3 A9' sera décodé comme deux caractères Latin-1, respectivement 'Ã' de code 'C3' et '©' de code 'A9'. Le caractère '€' sera décodé comme trois caractères Latin-1, respectivement 'â' de code 'E2', un caractère de contrôle de code '82', et 'Ì' de code 'AC'. Le caractère de contrôle s'affichera par exemple sous la forme '¿' : cela dépend du logiciel utilisé.__

__Le texte sera donc rendu "J'Ã©cris â¿Ì en UTF-8".__
