---
title: "Chapitre 6 : Boucles bornées"
subtitle: "Challenges TURTLE"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

__Réaliser les dessins représentés en utilisant le plus possible de boucles for__


# Challenge n°1 

![Challenge 1](./challenge_1.jpg)


# Challenge n°2 

![Challenge 2](./challenge_2.jpg)


# Challenge n°3  

![Challenge 3](./challenge_3.jpg)

_AIDE_ : vous utiliserez une fonction annexe permettant de dessiner un rectangle

# Challenge n°4  

![Challenge 4](./challenge_4.jpg)

# Challenge n°5  🥇🥇

![Challenge 5](./challenge_5.jpg)


