---
title: "Chapitre 14  : les dictionnaires en python "
subtitle: "Exercices"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

# Exercice 1 : le scrabble
 Le scrabble est un jeu de société où l'on doit former des mots avec un tirage aléatoire de lettres, chaque lettre valant un certain nombre de points.  
 Le tableau ci-dessous donne les points de certaines lettres :

 | Lettre | A | B | C | D | E | F | G | H | I | J | K |
|--------|---|---|---|---|---|---|---|---|---|---|---|
| Points | 1 | 3 | 3 | 2 | 1 | 4 | 2 | 4 | 1 | 8 | 5 |
 On considère le programme suivant :

```python
scrabble={"A":1,"B":3, "C":3,"D":2, "E":1, "F":4, "G":2,"H":4,"I":1, "J":8, "K":5}
somme=0
mot=input("Entrez un mot : ")

for i in mot :
    somme= somme + scrabble.get(i)
print (somme)
```

1. Quel est le type de la variable scrabble
    
2. Expliquer l'affectation de cette variable
   
3. Qu'affiche le programme si on entre le mot "KAKI"

4. Quel est le rôle de cette boucle?

# Exercice 2 :Tableau physico-chimique  

Le tableau suivant représente les données physico-chimiques de deux éléments chimiques. On donne la température d'ébullition Te, la température de fusion Tf , le numero atomique Z et le nombre de masse A.

| Elément | Te   | Tf   | Z  | A       |
|---------|------|------|----|---------|
| Au      | 2970 | 1063 | 79 | 196.967 |
| Ga      | 2237 | 29.7 | 31 | 69.72   |
 
1. Affectez des données de ce tableau à un dictionnaire Python de façon à pouvoir écrire par exemple :  
print (dico["Au"]["Z"])                           (affiche 79)

2. Afficher les clés de "dico"

3. Afficher toutes les clés de "Au"

4. Vérifier que les clés soient communes entre les deux éléments chimiques

# Exercice 3 : Moyennes

Soit le dictionnaire suivant listant pour chaque élève d'une classe, les notes d'une interrogation :
`interro = {"Kévin": 12, "Anissa": 15, "Mohmamed": 12, "Sophie": 18, "Grégory" : 14, "Yann" : 11, "Bryan": 07, "Lina" : 19, "Julien" : 17, "Tom" : 15} `

1. Calculer la moyenne des notes de l'interro en itérant sur les clés.
2. Calculer la moyenne des notes de l'interro en itérant sur les valeurs.
3. Quel est le nom de l'élève qui a la meilleure note (cas simple : on suppose qu'il n'y a qu'un premier de la classe) et quelle est sa note

Sources :
* Les vrais exos Nathan  NSI
* Prépas sciences NSI Ellipses
