---
title: "Chapitre 9-2 listes avancées "
subtitle: "Thème 2: Données type construit"
papersize: a4
geometry: margin=2cm
fontsize: 10pt
lang: fr
---

# Les listes par compréhension

## Différentes manières de construire des listes en Python

Pour le moment, nous avons vu plusieurs méthodes pour construire des listes. Nous allons voir l'utilité et les limites de chaque méthode.  

- Comment construire simplement une liste comprenant que des 0 ?
  
```python
   >>>[0]*10
   >>>[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
```

Cette méthode est compacte et simple à comprendre. Elle présente toutefois des limites.  
On ne peut remplir la liste qu'avec des valeurs identiques. Cette méthode est spécifique à Python.  

Remarque: Cette méthode présente des risques pour les listes à plusieurs dimensions. Nous éviterons donc de l'utiliser dans ces cas là.

- Comment construire simplement une liste comprenant des entiers croissants ?
  
```python
   >>>list(range(10))
   >>>[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
```

Là encore c'est une méthode simple et compacte mais limitée dans son usage.

- Pour la création d'une liste vide, nous avons déjà vu plusieurs méthodes qui se valent soit __[]__ soit __list()__

- Supposons maintenant que nous aimerions créer une liste de 10 éléments tous pairs.
  
```python
    liste = []
    for i in range(20):
        if not i%2:
            liste.append(i)
    print(liste)

    [0, 2, 4, 6, 8, 10, 12, 14, 16, 18]
```

On voit que lorsque les éléments d'une liste sont plus spécifiques, nous devons construire un programme plus élaboré et surtout moins compact.  

Les listes par compréhension permettent de retrouver cette compacité.  

## Les listes par compréhension.  

Rappelons que la principale utilité d'une liste par compréhension est de compacter le code. Pour le voir, nous allons reprendre les trois exemples ci-dessus et nous allons les traiter avec des listes par compréhension. (Cette méthode est spécifique à Python)  

__Construction d'une liste par compréhension:__
- On commence par ouvrir un crochet __[__
- On définit ensuite la valeur des cellules de la liste. 
- Ensuite, à l'aide d'une boucle for, on définit le nombre de cellules dans le tableau.
- Il est parfois nécessaire d'utiliser une structure conditionnelle.
- On termine par un crochet __]__  

Premier exemple: Créer une liste comprenant des 0.  
```python
   [0 for _ in range(10)]

   [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
```

Deuxième exemple: Créer une liste comprenant des entiers croissants.  
```python
   [i for i in range(10)]
   
   [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
```

Troisième exemple: Créer une liste comportant les 10 premiers entiers tous pairs.
```python
   [i for i in range(20) if not i%2]

   [0, 2, 4, 6, 8, 10, 12, 14, 16, 18]
```

Nous voyons au travers de ces exemples que les listes par compréhension se construisent sur une ligne.  
Elles peuvent construire des listes dont les éléments sont simples mais aussi complexes.  
Les listes par compréhension se construisent suivant le modèle __[ "element"  "boucle for" "structure conditionnelle si nécessaire"]__  

Remarque: Si la structure conditionnelle est de la forme if else, cette structure est placée avant la boucle for.  

# Les listes à plusieurs dimensions

Nous avons pour le moment utiliser des listes pour stocker des données de type simple.  
Nous allons maintenant construire une liste qui contient les notes d'une élève du lycée. Madeline a eu 15 notes pendant l'année à raison de 5 par trimestre.  
Nous souhaiterions mettre ces notes dans une seule liste mais pouvoir les regrouper par trimestre. Pour cela, il est possible de créer une liste de listes. Nous appellerons cette structure une liste à deux dimensions.
La première dimension est la liste qui regroupe les listes et la deuxième dimension les listes qui contiennent les notes.  

```python
   [[10, 12, 14, 8, 13], [11, 16, 9, 17, 6], [15, 18, 16, 14, 7]]
```

## Comment créer ce type de liste 

- On peut créer une liste à deux dimensions déjà pré-remplie.

```python
   liste = [[10, 12, 13], [8, 15, 14]]
```

- On peut utiliser une double boucle afin de créer une liste à deux dimensions

```python
1   liste = []
2   for j in range(3):
3       liste.append([])
4       for i in range(5):
5          liste[j].append(0)
6   print(liste)

   [[0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0]]
```

Cette méthode est intéressante car nous comprenons qu'à la ligne 1 nous créons une liste vide puis grâce à la première boucle (ligne 2), nous plaçons des listes dans la liste vide.  
La deuxième boucle (ligne 4) remplie ces listes de zéros.  

- Il est possible de réaliser la même chose de manière plus compacte avec des listes par compréhension.

```python
   liste = [[0 for i in range(5)] for j in range(3)]

   [[0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0]]
```

On commence par ouvrir deux crochets, on définit ensuite le contenu de chaque élément de la liste à deux dimensions puis comme dans l'exemple précédent on réalise une double boucle.
Remarque: Lorsque la liste contient des listes de taille identique, on peut la qualifier de __matrice__. Il est cependant tout à fait possible que la liste contiennent des listes de tailles différentes.

## Comment accéder à un élément de la liste à deux dimensions

On a la liste suivante:  
liste = [[12, 13, 14], [6, 16, 18], [9, 14, 17]]

Il faut bien comprendre que c'est une liste qui contient trois listes.  
Donc si on écrit liste[0], on aura comme résultat [12, 13, 14].  
Si on veut accéder à un élément de la liste [12, 13, 14], il faudra préciser son indice.  
Ainsi, liste[0][1] correspond au deuxième élément de la première liste.

```python
   liste = [[12, 13, 14], [6, 16, 18], [9, 14, 17]]
   liste[0][1]
   13

   liste[1][2]
   18
```

## Comment parcourir une liste à plusieurs dimensions

Il est utile de parcourir une liste à plusieurs dimensions pour l'afficher ou pour l'utiliser dans sa globalité.  
Pour cela, on est contraint de réaliser une double boucle.  

On réalise ici l'impression de la liste:  

```python
   liste = [[12, 13, 14], [6, 16, 18], [9, 14, 17]]
   for j in range(3):
      for i in range(3):
         print(liste[j][i], end=" ")
      print("")

   12 13 14 
   6 16 18 
   9 14 17 
```
