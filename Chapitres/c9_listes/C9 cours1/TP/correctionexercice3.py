from copy import copy

def creation_alphabet():
    """Fonction qui crée une liste comportant les lettres de l'alphabet."""
    return ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l",
            "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]

def decalage(n, param_alphabet):
    """Fonction qui renvoie une liste comportant l'alphabet décalé de n"""
    liste = copy(param_alphabet)
    for _ in range(n):
        dernier = liste[len(liste)-1]
        liste.insert(0, dernier)
        liste.pop()
    return liste

def demande_mot():
    """Fonction qui demande le mot à crypter."""
    demandemot = input("Quel est le mot à crypter?")
    return demandemot

def demande_cle():
    """Fonction qui demande le mot à crypter."""
    demandecle = int(input("Quel est la clé à crypter?"))
    return demandecle

def cryptage(param_lettre, param_alphabet, param_listedecalee):
    """Fonction qui fait le cryptage d'une lettre."""
    indice = param_alphabet.index(param_lettre)
    return param_listedecalee[indice]

def affichage(param_lettre):
    """Fonction qui affiche une lettre."""
    print(param_lettre, end = "")
    
def principale():
    """Fonction qui pilote le projet."""
    alphabet = creation_alphabet()
    valeur = demande_cle()
    liste_decalee = decalage(valeur, alphabet)
    mot = demande_mot()
    for lettre in mot:
        lettre_cryptee = cryptage(lettre, alphabet, liste_decalee)
        affichage(lettre_cryptee)


principale()
