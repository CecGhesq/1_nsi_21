# Créé par laurent, le 01/02/2022 en Python 3.7

from tkinter import*
from random import randint
import time

largeur=1600
hauteur=800

def tri_selection(tableau):
    global nbE
    rep=[(tableau[:],0,0,nbE,"depart")]
    rep.append((tableau[:],0,0,nbE,"depart"))
    nb = len(tableau)
    for en_cours in range(nb):
        plus_petit = en_cours
        rep.append((tableau[:],en_cours,plus_petit,nbE,"debut_plus_petit"))
        for j in range(en_cours+1,nb) :
            nbE+=1
            rep.append((tableau[:],en_cours,j,plus_petit,nbE,"test"))
            if tableau[j] < tableau[plus_petit] :
                plus_petit = j
                rep.append((tableau[:],en_cours,j,plus_petit,nbE,"etape"))
                rep.append((tableau[:],en_cours,j,plus_petit,nbE,"etapeb"))

        if min is not en_cours :
            nbE+=1
            for x in range ((plus_petit-en_cours)*4+1):
                rep.append((tableau[:],en_cours,plus_petit,x,nbE,"changement"))
            temp = tableau[en_cours]
            tableau[en_cours] = tableau[plus_petit]
            tableau[plus_petit] = temp

        rep.append((tableau[:],en_cours,plus_petit,nbE,"fin_changement"))
    rep.append((tableau[:],en_cours,plus_petit,nbE,"fin"))

    return rep

def Partie(x):
    global nbE,listenb,rep,liste,rang
    test=True
    nbE=0
    id=None
    enMarche=False
    listenb=[]

    def ListeNb(x):
        listenb=[]
        while len(listenb)!=16:
            nb=randint(-99,99)
            if nb not in listenb:
                listenb.append(nb)
        if x==2:
            listenb=sorted(listenb)
            x=False
        elif x==3:
            listenb=sorted(listenb,reverse=True)
            x=False
        bou1["state"]=DISABLED
        bou2["state"]=DISABLED
        bou3["state"]=DISABLED
        bou4["state"]=NORMAL
        bou5["state"]=NORMAL
        return listenb
    listenb=ListeNb(x)
    rep=tri_selection(listenb)
    liste=rep[0]
    rang=0

def jouer():
    global rep,liste,rang

    def bouger():
        global id,rang
        bou5["command"]=stop
        can.delete("all")
        rang+=1
        if rang<len(rep):
            liste=rep[rang]
            dessiner(liste)
            id=can.after(1000,bouger)
        else:
            liste=rep[len(rep)-1]
            dessiner(liste)
            test=False

    def testMarche():
            global enMarche,rang
            if not enMarche:
                    enMarche=True
                    bouger()

    def stop():
            global enMarche
            can.after_cancel(id)
            enMarche=False

    def dessiner(liste):
        global rang,rep
        a=(largeur-200)/16
        H=(hauteur+a)/2
        H2=H-a

        def rectangle(i,Width,Fill,Outline):
            can.create_rectangle(100+a*i,hauteur/2,100+a*(i+1),hauteur/2+a,width=Width,fill=Fill,outline=Outline)
        def texte(i,H,Text,Fill):
            can.create_text(100+a/2+a*i,H,text=Text,fill=Fill,font="Arial "+str(hauteur//22))
        def comparaison():
            can.create_text(largeur/2,hauteur/5,text="comparaisons/échanges :  "+str(liste[-2]),fill="#DABEA0",font="Arial "+str(hauteur//18))


        for i in range(16):
            rectangle(i,1,"ivory2","ivory3")
            texte(i,H,liste[0][i],"black")

        for i in range(16):
            comparaison()

            if i<liste[1]:
                rectangle(i,1,"ivory3","ivory4")
                texte(i,H,liste[0][i],"white")

            if liste[-1]=="debut_plus_petit":
                if i==liste[1]:
                    texte(i,H,liste[0][i],"red")
                    rectangle(i,3,"","red")

            if liste[-1]=="test":
                if i==liste[1]:
                    texte(i,H2,liste[0][i],"red")
                    rectangle(i,1,"ivory2","ivory3")
                elif i==liste[3]:
                    texte(i,H2,liste[0][i],"maroon")
                    rectangle(i,1,"ivory2","ivory3")
                elif i==liste[2]:
                    texte(i,H,liste[0][i],"green")
                    rectangle(i,3,"","green")

            if liste[-1]=="etape":
                if i==liste[1]:
                    texte(i,H2,liste[0][i],"red")
                    rectangle(i,1,"ivory2","ivory3")
                elif i==liste[2]:
                    texte(i,H,liste[0][i],"maroon")
                    rectangle(i,3,"","maroon")

            if liste[-1]=="etapeb":
                if i==liste[1]:
                    texte(i,H2,liste[0][i],"red")
                    rectangle(i,1,"ivory2","ivory3")
                elif i==liste[2]:
                    texte(i,H2,liste[0][i],"maroon")
                    rectangle(i,1,"ivory2","ivory3")

            if liste[-1]=="changement":
                if i==liste[1]:
                    texte(i+liste[-3]/4,H2,liste[0][i],"maroon")
                    rectangle(i,1,"ivory2","ivory3")
                elif i==liste[2]:
                    texte(i-liste[-3]/4,H2,liste[0][i],"red")
                    rectangle(i,1,"ivory2","ivory3")

            if liste[-1]=="fin_changement":
                if i==liste[1]:
                    texte(i,H,liste[0][i],"red")

            if liste[-1]=="fin":
                if i==liste[1]:
                    rectangle(i,1,"ivory3","ivory4")
                    texte(i,H,liste[0][i],"white")

    bouger()



fen=Tk()
xmax = fen.winfo_screenwidth()
ymax = fen.winfo_screenheight()
x0 =  xmax/2-(largeur/2)
y0 =  ymax/2-(hauteur/2)
fen.geometry("%dx%d+%d+%d" % (largeur,hauteur, x0, y0))
tex1 = Label(fen, text="\n <<oo>>  tri par sélection  <<oo>>",font ="Arial 35",fg="ivory4")
tex1.pack()
can=Canvas(height=hauteur-200,width=largeur)
can.pack()

bou1= Button(fen, text='QUELCONQUE', bg="ivory3", fg="white",font="Arial  25",bd=0,command=lambda:Partie(1))
bou1.pack(side=LEFT,expand=YES, fill=BOTH)

bou2= Button(fen, text='CROISSANT',  bg="ivory3", fg="white",font="Arial  25",bd=0,command=lambda:Partie(2))
bou2.pack(side= LEFT,expand=YES, fill=BOTH)

bou3= Button(fen, text='DECROISSANT', bg="ivory3", fg="white",font="Arial  25",bd=0,command=lambda:Partie(3))
bou3.pack(side=LEFT,expand=YES, fill=BOTH)

bou4= Button(fen, text='START',  bg="ivory3", fg="white",font="Arial  25",bd=0,state =DISABLED,command=jouer)
bou4.pack(side= LEFT,expand=YES, fill=BOTH)

bou5= Button(fen, text='PAUSE',  bg="ivory3", fg="white",font="Arial  25",bd=0,state =DISABLED)
bou5.pack(side= LEFT,expand=YES, fill=BOTH)

fen.mainloop()




