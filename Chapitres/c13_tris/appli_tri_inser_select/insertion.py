# Créé par laurent, le 29/01/2022 en Python 3.7
from tkinter import*
from random import randint
import time
largeur=1600
hauteur=800

def tri_insertion(L):
    global nbE
    rep=[(L[:],0,0)]
    rep.append((L[:],0,0))
    rep.append((L[:],0,0,nbE,"x"))
    rep.append((L[:],0,0,nbE,"b"))
    N = len(L)
    for i in range(1,N):
        rep.append((L[:],i,i,nbE,"x"))
        j = i-1
        idep=i
        while j>=0 and L[j] > L[j+1]:
            nbE+=1
            rep.append((L[:],j,idep,nbE,"a"))

            L[j+1],L[j]= L[j],L[j+1] # decalage


            rep.append((L[:],j,i,idep,nbE,""))
            nbE+=1
            rep.append((L[:],j,idep,nbE,"x"))

            j = j-1
        if j>=0:
            nbE+=1
        rep.append((L[:],j+1,i,nbE,"b"))
    rep.append((L[:],i,nbE,"e"))
    return rep

def Partie(x):
    global nbE,listenb,rep,liste,rang
    test=True
    nbE=0
    id=None
    enMarche=False
    listenb=[]

    def ListeNb(x):
        listenb=[]
        while len(listenb)!=16:
            nb=randint(-99,99)
            if nb not in listenb:
                listenb.append(nb)
        if x==2:

            listenb=sorted(listenb)
            x=False
        elif x==3:
            listenb=sorted(listenb,reverse=True)
            x=False
        bou1["state"]=DISABLED
        bou2["state"]=DISABLED
        bou3["state"]=DISABLED
        bou4["state"]=NORMAL
        bou5["state"]=NORMAL
        return listenb
    listenb=ListeNb(x)
    rep=tri_insertion(listenb)
    liste=rep[0]
    rang=0

def jouer():
    global rep,liste,rang

    def bouger():
        global id,rang
        bou5["command"]=stop
        can.delete("all")
        rang+=1
        if rang<len(rep):
            liste=rep[rang]
            dessiner(liste)
            id=can.after(1000,bouger)
        else:
            liste=rep[len(rep)-1]
            dessiner(liste)
            test=False

    def testMarche():
            global enMarche,rang
            if not enMarche:
                    enMarche=True
                    bouger()

    def stop():
            global enMarche
            can.after_cancel(id)
            enMarche=False

    def dessiner(liste):
        global rang,rep
        a=(largeur-200)/16
        H=(hauteur+a)/2
        H2=H-a

        def rectangle(i,Width,Fill,Outline):
            can.create_rectangle(100+a*i,hauteur/2,100+a*(i+1),hauteur/2+a,width=Width,fill=Fill,outline=Outline)
        def texte(i,H,Text,Fill):
            can.create_text(100+a/2+a*i,H,text=Text,fill=Fill,font="Arial "+str(hauteur//22))
        def comparaison():
            can.create_text(largeur/2,hauteur/5,text="comparaisons/échanges :  "+str(liste[-2]),fill="#DABEA0",font="Arial "+str(hauteur//18))

        for i in range(16):
            rectangle(i,1,"ivory2","ivory3")
            texte(i,H,liste[0][i],"black")
        for i in range(16):
            comparaison()

            if liste[-1] in ["","a","b","e"]:
                if i<=liste[1]:
                    rectangle(i,1,"ivory3","ivory4")
                    texte(i,H,liste[0][i],"white")

            if liste[-1]=="":
                if i==liste[1]:
                    rectangle(i,1,"ivory2","ivory3")
                    texte(i,H2,liste[0][i],"red")
                elif i==liste[1]+1:
                    rectangle(i,1,"ivory3","ivory4")
                    texte(i,H,liste[0][i],"green")
                    rectangle(i,3,"","green")
                elif i<=liste[2]:
                    rectangle(i,1,"ivory3","ivory4")
                    texte(i,H,liste[0][i],"white")
                rectangle(liste[1]+1,3,"","green")

            elif liste[-1]=="a":
                if i==liste[1]:
                    rectangle(i,1,"ivory3","ivory4")
                    texte(i,H,liste[0][i],"green")
                    rectangle(i,3,"","green")
                elif i==liste[1]+1:# si on est au rang j
                    rectangle(i,1,"ivory2","ivory3")
                    texte(i,H2,liste[0][i],"red")
                elif i<=liste[2]:
                    rectangle(i,1,"ivory3","ivory4")
                    texte(i,H,liste[0][i],"white")

            elif liste[-1]=="b":
                if i==liste[1]-1:
                    rectangle(i,1,"ivory3","ivory4")
                    texte(i,H,liste[0][i],"green")
                elif i==liste[1]:
                    texte(i,H,liste[0][i],"red")
                elif i==liste[1]+1 and liste[1]<liste[2]:
                        rectangle(i,1,"ivory3","ivory4")
                        texte(i,H,liste[0][i],"white")
                        if i>1:
                            rectangle(i-2,3,"","green")
                elif i<=liste[2]:
                    rectangle(i,1,"ivory3","ivory4")
                    texte(i,H,liste[0][i],"white")
                rectangle(liste[1],3,"","maroon")

            elif liste[-1]=="x":
                if liste[1]==i:
                    texte(i,H,liste[0][i],"red")
                else:
                    if i<=liste[2]:
                        rectangle(i,1,"ivory3","ivory4")
                        texte(i,H,liste[0][i],"white")

    bouger()

fen=Tk()
xmax = fen.winfo_screenwidth()
ymax = fen.winfo_screenheight()
x0 =  xmax/2-(largeur/2)
y0 =  ymax/2-(hauteur/2)
fen.geometry("%dx%d+%d+%d" % (largeur,hauteur, x0, y0))
tex1 = Label(fen, text="\n <<oo>>  tri par insertion  <<oo>>",font ="Arial 35",fg="ivory4")
tex1.pack()
can=Canvas(height=hauteur-200,width=largeur)
can.pack()
bou1= Button(fen, text='QUELCONQUE', bg="ivory3", fg="white",font="Arial 20",bd=0,command=lambda:Partie(1))
bou1.pack(side=LEFT,expand=YES, fill=BOTH)

bou2= Button(fen, text='CROISSANT',  bg="ivory3", fg="white",font="Arial 20",bd=0,command=lambda:Partie(2))
bou2.pack(side= LEFT,expand=YES, fill=BOTH)

bou3= Button(fen, text='DECROISSANT', bg="ivory3", fg="white",font="Arial 20",bd=0,command=lambda:Partie(3))
bou3.pack(side=LEFT,expand=YES, fill=BOTH)

bou4= Button(fen, text='START',  bg="ivory3", fg="white",font="Arial 20",bd=0,state =DISABLED,command=jouer)
bou4.pack(side= LEFT,expand=YES, fill=BOTH)

bou5= Button(fen, text='PAUSE',  bg="ivory3", fg="white",font="Arial 20",bd=0,state =DISABLED)
bou5.pack(side= LEFT,expand=YES, fill=BOTH)

fen.mainloop()


