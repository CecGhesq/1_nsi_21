
# <center> TP : les propriétés en algèbre de Boole et les portes logiques </center>

## 1. Utilisation de CircuitVerse

vous allez utiliser un simulateur de circuit électronique en ligne : [CircuitVerse](https://circuitverse.org/simulator).
Afin de se familiariser avec son fonctionnement, choisir le menu `help` dans la barre des menus et explorer le dossier `Getting Started`
![Découverte du logiciel](./img_tp/decouverte_logiciel_rz.png)




## 2. Porte AND,  associativité et commutativité
### 2.1. Porte AND 
1. Réaliser dans CircuitVerse une porte AND avec trois entrées et compléter la table de vérité ci-dessous :   
Ne pas oublier de modifier les propriétés afin d'avoir trois entrées :   

|![propriétés](./img_tp/prop_rz.png)|![and](./img_tp/and_rz.png)|
|:---:|:---:|  


|a|b|c|$$s = a \land b \land c$$|
|:---:|:---:|:---:|:---:|
|0|0|0|0|
|0|0|1|0|
|0|1|0|0|
|0|1|1|0|
|1|0|0|0|
|1|0|1|0|
|1|1|0|0|
|1|1|1|1|  



 <p style="border: 1px solid red"> Conclusion : On retient que la porte AND permet de tester quand toutes les entrées sont vraies</p> 

### 2.2 L'associativité  

Réaliser l'expression suivante  de deux façons différentes dans CircuitVerse( avec l'autre notation  s = a.b.c )  :$$s=a\land b \land c $$ 
<img src="./img/associativite.png" width="50%"/>
Utiliser le menu `misc` afin de nommer les entrées et sorties.  
![associativité](./img_tp/associa.png)

Tester avec différentes valeurs d'entrées.  
<p style="border: 1px solid red"> Conclusion : On retient la propriété d'associativité : (a.b).c = a.(b.c)</p> 


### 2.3 La commutativité

Réaliser s = a.b puis s = b.a  dans CircuitVerse: 
<img src="./img/commutativite.png" width="40%"/>
Tester avec différentes valeurs d'entrées et enregistrer votre fichier associativite.crc dans votre dossier.  
<p style="border: 1px solid red"> Conclusion : On retient la propriété de commutativite : a.b = b.a</p> 


## 3. Porte OR,  associativité et commutativité
### 3.1. Porte OR 
1. Réaliser dans Logisim un porte OR avec trois entrées et compléter la table de vérité ci-dessous :   

|a|b|c|$$s = a\lor b \lor c$$|
|:---:|:---:|:---:|:---:|
|0|0|0|0|
|0|0|1|1|
|0|1|0|1|
|0|1|1|1|
|1|0|0|1|
|1|0|1|1|
|1|1|0|1|
|1|1|1|1|    


 <p style="border: 1px solid red"> Conclusion : On retient que la porte OR permet de tester quand au moins une des entrées est vraie</p> 

### 3.2. L'associativité et la commutativité
On retrouve les propriétés précédentes pour la fonction logique OR : 
<img src="./img/associativite_or.png"/>

Testez-le en ouvrant le fichier: [associativite_or](https://circuitverse.org/simulator/edit/32721)

## 4 . Porte NAND et théorème de Morgan
### 4.1. La solution liée au code
On reprend le code : on fait un AND et on le complémente.
<img src="./img/NAND.png" width = "50%"/> 
Le créer dans CircuitVerse puis remplir la table de vérité :  

|a|b|$$ s =\overline {a \land b}$$ |
|:---:|:---:|:---:|
|0|0|0|
|0|0|1|
|0|1|0|
|0|1|1|
|1|0|0|
|1|0|1|
|1|1|0|
|1|1|1|  



### 4.2. Théorème de Morgan 
Il existe une autre solution qu'on peut trouver en utilisant les lois de De Morgan.

Pour faire simple : lorsqu'on complémente l'opération (a ET b), on peut obtenir le même résultat en utilisant un OU sur les états complémentés de  a et b.
En équation, ca donne : $$\overline{a \land b} = \overline{a} \lor \overline {b}$$

On voit que la conjonction (and) devient une disjonction (or).

Trouvez donc l'agencement des portes et réalisez-le dans CircuitVerse : on doit retrouver la même table de vérité.

<p style="color: red"> Conclusion : Le théorème de Morgan a pour expression :  
 $$\overline{a \land b} = \overline{a} \lor \overline {b}$$
 $$\overline{a \lor b} = \overline{a} \land \overline {b}$$
 </p>   
 
[De Morgan](https://circuitverse.org/simulator/edit/32735)  

 On peut aussi le faire avec des portes NOR ( cf cours)

## 5 Les autres propriétés d'algèbre de Boole
### 5.1. La complémentarité
On appelle complémentarité not(not(a)) vaut a.
Mathématiquement, cela donne :  
<span style= "color:red">$$
 \overline{\overline{a}} = a $$ </span>



```python
a = True
print(a)
```

    True
    


```python
print( not(not a ))
```

    True
    


```python
a = False
print( a and (not(a)))
```

    False
    

Dans le code précédent que a soit vrai ou faux  ( à tester!!!), son complémentaire ne peut l'être aussi ( vrai ou faux dans cet ordre). Donc la fonction `and` renverra toujours faux. En algèbre on l'écrit :    
    <span style= "color:red">$$ a.\overline{a} = 0 $$ </span>

Si on teste maintenant la complémentarité de la fonction `or` : 


```python
a = True
print(a or (not(a)))
```

    True
    

Cette fois que a soit vrai ou faux  ( à tester!!!),  la fonction `or` renverra toujours vrai. En algèbre on l'écrit :    
<span style= "color:red"> $$ a+\overline{a} = 1 $$ </span>

## 5.2. L'élement neutre et l'absorption

* Influence du VRAI (1)

<span style="color : red" >a . 1   =   a  </span>     : 1 est neutre dans un ET car c'est a qui impose le résultat.

<span style="color : red" >a + 1   =   1  </span>     : 1 absorbe le résultat d'un OU en imposant une valeur VRAI (1) en sortie.

 * Influence du FAUX (0)

<span style="color : red" >a . 0   =   0 </span>      : 0 absorbe le résultat d'un ET en imposant une valeur FAUX (0) en sortie.

<span style="color : red" >a + 0   =   a  </span>     : 0 est neutre dans un OU car c'est a qui impose le résultat.



```python
a = True
print(a and (False))
```

    False
    


```python
a = True
print(a or (False))
```

    True
    

### 5.3. L'idempotence 
Le nom est à lire ainsi : idem potence ; donc quelle est l'influence d'utiliser le même opérateur?
Dans le cas qui nous concerne, cela veut dire qu'appliquer la fonction une fois ou plus ne change pas l'état de sortie.  

Dans le cas du `and` :   (a and a)  donne a. Mathématiquement a.a = a
<img src = "./img/idempotence_and.png" width = "20%"/>
Dans le cas du `or` :  (a or a) donne a. Mathématiquement a + a = a
<img src = "./img/idempotence_or.png" width = "20%"/>


```python
a = True
print(a and a and a and a)
```

    True
    


```python
a = True
print (a or a or a or a or a)
```

    True
    

### 5.4 La distributivité

Commençons par la **distributivité que l'on connaît** puisque la fonction `and` est représentée par une multiplication de booléens.
Mathématiquement, cela donne :

$$ a \land ( b\lor c ) = ( a \land b ) \lor ( a \land c ) $$

En logique booléenne, on remplace la multiplication par le  `and`et l'addition par le `or` et cela donne :

a and ( b or c ) équivalent à ( a and b ) or ( a and c ).  
On peut vérifier la propriété à l'aide du fichier(onglet and) : [distributivité_and](https://circuitverse.org/simulator/edit/33092)

Ce deuxième cas de **distributivité est plus "troublant"** car il n'est pas vrai en algèbre classique :

Mathématiquement, cela donne :

a + ( b.c ) = ( a + b ) . ( a + c ).

En logique booléenne, on remplace la multiplication par  `and` et l'addition par  `or` et cela donne :

a or ( b and c ) équivalent à ( a or b ) and ( a or c ).
On peut vérifier la propriété à l'aide du fichier (onglet and) : [distributivité_or](https://circuitverse.org/simulator/edit/33092)

## 6 Testons des portes logiques....
### 6.1. La planche à pain ou bread bord....
<img src="./img/bread_bord.png" width = "70%" />
* Tous les points désignés par des lettres majuscules A, B .. sont des lignes verticales au même potentiel   

* Les points désignés par chiffres 12, 14 ... sont des lignes horizontales au même potentiel.

### 6.2 Le circuit  imprimé
<table>
    <tr> 
        <td style="text-align: left" width = "70%" >
Quatre portes logiques identiques sont placées dans un circuit intégré qui possède 14 bornes. Une encoche permet de les implanter dans le bon sens dans un circuit.    
    <ul>
        <li> Deux de ces bornes, n°7 et n°14, servent à l’alimentation du circuit intégré </li>
        <li>3 bornes pour chacune des portes logiques </li>
<li> Chaque porte logique a deux entrées (notées E) et permet donc de réaliser une fonction logique à deux variables logiques comme par exemple les fonctions logiques « and » et « or » vues précédemment.  </li>
     </ul>
        </td>
        <td> 
            <img src="./img/circuit_integre.png" width = "70%" />
       </td>
    </tr> 
   <tr> 
        <td style="text-align: left" width = "80%">
Tout circuit intégré doit d'abord être alimenté. Il ne peut fonctionner sans alimentation.
        <ul>
            <li>Dans un premier temps, on se propose de reconnaître les fonctions logiques réalisées par différents circuits intégrés de références :74 HC 00 ; 74 HC 02 ; 74 HC 08 ; 74 HC 32 ; 74 HC 86 </li>
            <li>Les broches d'entrée doivent être connectées soit à la borne +(état 1), soit à la masse (état 0), et ne doivent jamais rester « en l'air » </li>
            </ul>
        </td>
        <td> <img src= "./img/montage_porte.png" width ="80%"/>
       </td>
  </tr> 
</table>

### 6.3 Identification des portes logiques
* Pour une porte logique d’un circuit intégré, réaliser le montage ci-contre avec le plus grand soin. Alimenter le circuit intégré. Puisimposer aux entrées des tensions hautes ou basses correspondantrespectivement aux états logiques 0 et 1.
* L’état 0 des entrées E1 et E2 sera obtenu pour U1 et U2 égales à 0 V (pattes d’entrées reliées à la masse).
* L’état 1 des entrées E1 et E2 sera obtenu pour U1 et U2 égales à une tension de 5 V(pattes d’entrées reliées à l’alimentation)
* L’état logique de la sortie sera connu grâce à la DEL. 
* On remplira une table de vérité pour chacun des circuits intégrés testés. 
* Identifier la porte logique testée
<img src="./img/table_verite.png"/>


Sources : https://infoforall.fr/fiches/fiches-act080.html#partie_01  
http://a.bougaud.free.fr/SECmpi/TP15_portes_logiques.pdf


```python

```
