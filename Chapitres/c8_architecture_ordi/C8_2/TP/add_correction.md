---
title: "Chapitre 8_2 : Algèbre de Boole et circuits combinatoires"  
subtitle: "Chapitre 8 : Architecture de l'ordinateur"
papersize: a4  
geometry: margin=1.5cm  
fontsize: 12pt 
output: pdf_document 
lang: fr  
---

## 3. L'additionneur  
### 3.1. Le demi-additionneur  
Commençons par l’addition de deux bits a et b en entrée, avec en sortie la somme S et la retenue R. Cela s’appelle le `demi-additionneur`, parce qu’il ne tient pas compte de la retenue qui peut aussi arriver en entrée, provenant de calculs précédents.
* Remplir la table de vérité suivante :

|a|b|s|r|
|---|---|---|---|
|0|0|0|0|
|0|1|1|0|
|1|0|1|0|
|1|1|0|1|



* En déduire les expressions booléennes pour S et R :
$`S = a⊕b `$ on retrouve le `ou exclusif`
$`R = a \land b `$

* Combien de portes vont-elles être nécessaires?
--> une porte AND et une porte XOR

* Réaliser le circuit dans CircuitVerse et réaliser des copies d'écran pour les différentes situations.
![demi_add](./img_tp/demi-add.jpg)  

[lien](https://urlz.fr/gUFL)

### 3.2. L'additionneur 1bit complet
On  tient  maintenant  compte  de  la  retenue r  provenant  éventuellement  d’un  calcul  en  amont. L’additionneur complet a trois entrées a, b et r, ainsi que deux sorties S et R. 
* Compléter la table de vérité :

|a|b|r|S|R|
|---|---|---|---|---|
|0|0|0|0|0|
|0|0|1|1|0|
|0|1|0|1|0|
|0|1|1|0|1|
|1|0|0|1|0|
|1|0|1|0|1|
|1|1|0|0|1|
|1|1|1|1|1|  

* Etablir les expressions booléennes S et R ( en utilisant l'autre notation):  

$` S = \overline{a}.\overline{b}.r + \overline{a}.b.\overline{r} + a.\overline{b}.\overline{r} + a.b.r `$  
$` R = \overline{a}.b.r + a.\overline{b}.r + a.b.\overline{r} + a.b.r `$  
qui peuvent être  simplifiées  :  
$` S = \overline{r}.(\overline{a}.b + a.\overline{b}) + r.(a.b + \overline{a}.\overline{b}) `$  
$` S = \overline{r}.(a ⊕ b) + r.(\overline{a ⊕ b}) `$  
$` S = r ⊕ (a ⊕ b) = r ⊕ a ⊕ b `$  


$` R = ab + ar + br `$  
$` R = ab + r.(a ⊕ b) `$  

* Si on se limite à utiliser les portes AND, OR et NOT : combien sont nécessaires pour réaliser S?  

pour S : 3 NOT, 1 OR à 4 entrées et 4 AND à 3 entrées

* on va utiliser les deux demi-additionneurs pour le réaliser avec les portes XOR ; 

[lien vers l'additionneur](https://urlz.fr/gUFU)

