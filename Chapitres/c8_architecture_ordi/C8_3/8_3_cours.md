---
title: "Chapitre 8 : Architecture de l'ordinateur"  
subtitle: "Chapitre 8_3 : Le langage assembleur"
papersize: a4  
geometry: margin=1.5cm  
fontsize: 12pt  
lang: fr  
---

# Introduction

Le langage assembleur est le langage de plus bas niveau qui représente le langage machine compréhensible pour l'homme. C'est un langage qui permet de contrôler l'espace mémoire mais aussi le code qui est executé par le microprocesseur. Pour exemple, un affichage du texte "Hello World" prend 15839 octets en langage C alors qu'il n'en prend que 23 en assembleur. En contre partie, la moindre erreur peut provoquer un gros disfonctionnement machine.

# Pourquoi conserver le langage assembleur?

## Les différents niveaux de langage

- Les langages de bas niveau :  
Ce sont des langages qui sont très proches de la machine. Ainsi, si vous voulez programmer en langage assembleur sur votre ordinateur, vous devrez connaitre les instructions spécifiques à votre microprocesseur. Vous pourrez contrôler l'ensemble des paramètres de votre microprocesseur mais aussi de vos lecteurs, le CPU et la carte graphique. Cependant, le jeu d'instructions est très important et la programmation en assembleur est assez lourde en lignes de code. Les premières architectures comprenaient environ 30 instructions dans le jeu de commande. Actuellement, les derniers microprocesseurs contiennent plus de mille instructions.

- Les langages de haut niveau:  
Javascript, Python, Java, PHP, C, C++ ... sont classés dans les langages haut niveau. Ils sont universels et ne dépendent pas de l'architecture de votre ordinateur. Ils sont plus souples d'utilisation car ils permettent d'obtenir le même résultat qu'en langage assembleur mais en moins de lignes. Cependant, ces langages qu'ils soient compilés ou interprétés, seront toujours traduits en langage machine avant d'être exécutés par le microprocesseur. La performance de votre programme dépendra donc fortement de la qualité de la traduction en langage machine.  

Conclusion: On distingue bien les niveaux des langages et les avantages et inconvénients de chacun.

## Pourquoi conserver le langage assembleur

- Comme nous l'avons vu dans le paragraphe précedent, l'asembleur est très proche du langage machine donc sa traduction est rapide et fidèle au code. Pour les langages de haut niveau, la performance dépend de la traduction et des choix du traducteur. Cependant, ces traducteurs sont de plus en plus performants et l'écart de performance entre les langages haut et bas niveaux tend à se réduire.

- La deuxième raison qui est certainement la plus pertinente est qu'actuellement certains programmes réalisés en assembleur ne peuvent pas être faits en langage haut niveaux. L'exemple le plus marquant est la gestion du microprocesseur par windows qui se fait encore à partir de programmes en assembleur.

# Introduction au langage d'assemblage

## Architecture machine  

- Dans la machine débranchée que nous allons utiliser, il y a une UAL (unité arithmétique et logique) qui est en charge de tous les calculs au sein du microprocesseur.

- Nous avons aussi un accumulateur qui est un registre (Mémoire très rapide et très proche de l'UAL) particulier qui reçoit les résultats produit par l'UAL.

- Enfin nous avons des registres qui stockent de façon très brêve les informations utiles aux calculs de l'UAL. Ici, nous prendrons deux registres A et B qui représenteront les entrées de l'UAL.

- Nous avons la mémoire qui permet un stockage plus important mais dont l'accès est plus lent.

- Enfin, nous aurons un compteur qui permet de rythmer et repérer notre position dans le déroulement du programme.

![schéma de la machine](./images/schemamachine.png)

## Jeu d'instruction pour utiliser cette machine

Chaque machine que vous allez rencontrer aura un jeu d'instructions spécifiques au microprocesseur. Ce jeu d'instructions représente les différentes actions que pourra réaliser la machine proposée.

Pour écrire un programme en langage assembleur, on utilise des mots appelés **mnémoniques**. Ainsi, pour notre machine, nous utiliserons le jeu d'instructions ci-dessous:  

![Jeu d'instructions](images/jeuinstructions1.png)  

On affecte aussi des valeurs au registre:  

![Titre](images/valeurregistre.png)  

Pour comprendre comment on code en langage assembleur, nous allons commenter certaines instructions.  
Exemples:  

- **LDA 12** se traduira d'après la tableau par **012** (0 est le code attribué à LDA) ce qui veut dire charger la valeur contenue dans l'adresse mémoire 12 dans le registre A.  

- **JMP 14** se traduira par **514**. Cela consiste à réaliser un saut de l'adresse où l'on se trouve jusqu'à l'adresse 14.

- On suppose que le registre A contient la valeur 5 et le registre B la valeur 8. L'instruction **ADD 00** se traduira par **300**. Cela va réaliser l'addition du contenu de A et B et le mettre dans R. Donc R contiendra la valeur 13.

## Comment se déroule un programme en langage assembleur?  

- Première chose importante, on ne peut pas créer de nouvelles instructions. On ne doit utiliser que le jeu qui nous est fourni.  

- Un programme en assembleur se déroule de façon linéaire. On lit les instructions dans l'ordre. Elles sont placées les unes en desous des autres.

# La machine M999

La machine M999 est une architecture qui comporte 99 emplacements mémoire, deux registres A et B qui forment l'entrée de l'UAL, une UAL, un accumulateur R qui reçoit le résultat de l'UAL et un compteur.  
Remarque: La case **99** est particulière car elle réalise l'affichage et stoppe le programme.  

Voici le schéma de la machine M999:  

![M999](images/m999.png)  

Nous allons voir le fonctionnement de cette machine sur un exemple:  

![exemple addition](images/exempleaddition.png)

**Le compteur passe à 1:**
  
- On lit l'instruction dans la mémoire d'adresse 0.  

- On a la valeur 010 ce qui se traduit par 0 et 10 soit LDA 10.

- Charger la valeur de l'adresse mémoire 10 dans le registre A.  

![exemple addition 1](images/exempleadd1.png)  
  
**Le compteur passe à 2:**  

- On lit l'instruction dans la mémoire d'adresse 1.  

- On a la valeur 111 ce qui se traduit par 1 et 11 soit LDB 11.  

- Charger la valeur de l'adresse mémoire 11 dans le registre B.  

![Exemple addition 2](images/exempleadd2.png)  

**Le compteur passe à 3:**  

- On lit l'instruction dans la mémoire d'adresse 2.  

- On a la valeur 300 ce qui se traduit par 3 et 00 soit ADD 00.

- On ajoute les valeurs des registres A et B et on produit le résultat dans R.  

- L'UAL réalise l'opération 25 + 124 = 149 et met cette valeur dans R.  
  
![Exemple addition 3](images/exempleadd3.png)  

**Le compteur passe à 4:**  

- On lit l'instruction dans la mémoire d'adresse 3.

- On a la valeur 212 ce qui se traduit par 2 et 12 soit STR 12.  

- On copie le contenu du registre R dans la mémoire d'adresse 12.  
  
![Exemple addition 4](images/exempleadd4.png)  

**Le compteur passe à 5:**  

- On lit l'instruction dans la mémoire d'adresse 4.

- On a la valeur 599 ce qui se traduit par 5 et 99 soit JMP 99.  

- On réalise un saut vers la mémoire d'adresse 99. Arrêt du programme.  
  
![exemple addition 5](images/exempleadd5.png)

On résume notre programme:  
 1 LDA 10  
 2 LDB 11  
 3 ADD 00  
 4 STR 12  
 5 JMP 99  
Ce programme réalise l'addition des mémoires d'adresse 10 et 11 puis écrit le résultat dans la mémoire d'adresse 12 puis arrêt du programme.
